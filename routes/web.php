<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\MainController;
use \App\Http\Controllers\AccountController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class,'index'])->name('home');
Route::get('/products', [MainController::class,'products'])->name('products');
Route::get('/products/{slug}', [MainController::class,'products_inner'])->name('products_inner');
Route::get('/news', [MainController::class,'news'])->name('news');
Route::get('/news/{slug}', [MainController::class,'news_inner'])->name('news_inner');
Route::get('/about', [MainController::class,'about'])->name('about');
Route::get('/vacancy', [MainController::class,'vacancy'])->name('vacancy');
Route::get('/gallery', [MainController::class,'gallery'])->name('gallery');
Route::get('/contact', [MainController::class,'contact'])->name('contact');
Route::post('/send-mail', [MainController::class,'send_email']);
Route::post('/send-order', [MainController::class,'send_order']);

Route::get('login', [AccountController::class,'login'])->name('login');
Route::post('login', [AccountController::class,'signin'])->name('signin');
//authentificated
Route::group(['middleware' => ['auth']], function () {
    Route::get('apanel',[AccountController::class,'index']);
    Route::get('apanel/home-page-gallery',[AccountController::class,'home_page_gallery']);

//section content
    Route::get('apanel/section-work',[AccountController::class,'section_work_content']);
    Route::get('apanel/section-content',[AccountController::class,'section_static_content']);
    Route::post('apanel/section-content',[AccountController::class,'translate_static_content']);
    Route::get('apanel/section-products',[AccountController::class,'section_products_content']);
    Route::get('apanel/section-notes',[AccountController::class,'section_notes_content']);
    Route::get('apanel/section-categories',[AccountController::class,'section_categories_content']);
    Route::get('apanel/section-vacancies',[AccountController::class,'section_vacancies_content']);
    Route::get('apanel/section-services',[AccountController::class,'section_services_content']);
    Route::get('apanel/section-orders',[AccountController::class,'section_orders_content']);
    Route::get('apanel/section-news',[AccountController::class,'section_news_content']);
    Route::post('apanel/sectionapplystyle',[AccountController::class,'section_apply_style']);
//
    Route::get('apanel/sectionarticle',[AccountController::class,'article_content']);
    Route::post('apanel/deletepostfromsection',[AccountController::class,'delete_post_from_section']);
    Route::post('apanel/deleteproductfromsection',[AccountController::class,'delete_product_from_section']);
    Route::post('apanel/deletevacancyfromsection',[AccountController::class,'delete_vacancy_from_section']);
    Route::post('apanel/deletenotefromsection',[AccountController::class,'delete_note_from_section']);
    Route::post('apanel/deleteservicefromsection',[AccountController::class,'delete_service_from_section']);
    Route::post('apanel/deletegalleryfromsection',[AccountController::class,'delete_gallery_from_section']);
    Route::post('apanel/deletepagegalleryfromsection',[AccountController::class,'delete_page_gallery_from_section']);
    Route::post('apanel/deletearticlefrompost',[AccountController::class,'delete_article_from_post']);
    Route::get('apanel/drafts',[AccountController::class,'drafts']);
    Route::get('apanel/preview/{slug}',[AccountController::class,'preview_draft']);

    Route::get('apanel/translates/{post_id}/{langID}',[AccountController::class,'translates'])->name('translated_post');
    Route::post('apanel/translates/updatetranslate/{post_id}/{langID}',[AccountController::class,'update_translates'])->name('update_translates');

    Route::get('apanel/translatetext/{text_id}/{langID}',[AccountController::class,'text_translates'])->name('translated_text');
    Route::post('apanel/translates/updatetexttranslate/{text_id}/{langID}',[AccountController::class,'update_text_translates'])->name('update_text_translates');

    Route::get('apanel/post/{slug}',[AccountController::class,'post']);
    Route::get('apanel/service/{id}',[AccountController::class,'service']);
    Route::get('apanel/products/{slug}',[AccountController::class,'products']);
    Route::get('apanel/notes/{id}',[AccountController::class,'notes']);
    Route::get('apanel/vacancies/{id}',[AccountController::class,'vacancies']);
    Route::post('apanel/changeorder',[AccountController::class,'changeorder']);
    Route::post('apanel/changeorderproducts',[AccountController::class,'changeorder_products']);
    Route::post('apanel/changeorderservices',[AccountController::class,'changeorder_services']);
    Route::post('apanel/changeordervacancies',[AccountController::class,'changeorder_vacancies']);
    Route::post('apanel/changeordergallery',[AccountController::class,'changeorder_gallery']);
    Route::post('apanel/changeorderpagegallery',[AccountController::class,'changeorder_pagegallery']);
    Route::post('apanel/changeorderarticle',[AccountController::class,'changeorder_article']);

    /*
     * Inner Post slider
    */
    Route::post('apanel/post/uploadsliderimage',[AccountController::class,'upload_slider_image']);
    Route::post('apanel/post/ordersliderimage',[AccountController::class,'order_slider_image']);
    Route::post('apanel/post/deletesliderimage',[AccountController::class,'delete_slider_image']);

    /*
     * Post's IMAGE manipulation
    */

    Route::post('apanel/post/changeimageorder',[AccountController::class,'change_image_order']);
    Route::post('apanel/post/makedefaultimage',[AccountController::class,'make_default_image']);
    Route::post('apanel/post/deleteimage',[AccountController::class,'delete_image']);
    Route::post('apanel/post/uploadimage',[AccountController::class,'upload_image']);
    Route::post('apanel/post/uploadgallery',[AccountController::class,'upload_gallery']);
    Route::post('apanel/post/uploadpagegallery',[AccountController::class,'upload_page_gallery']);
    /*
     * Post Manipulation
     */
    Route::get('apanel/add-news',[AccountController::class,'add_news']);//insert post
    Route::get('apanel/add-logo',[AccountController::class,'add_logo']);//insert post
    Route::get('apanel/add-products',[AccountController::class,'add_products']);//insert post
    Route::get('apanel/add-notes',[AccountController::class,'add_notes']);//insert post
    Route::get('apanel/add-vacancies',[AccountController::class,'add_vacancies']);//insert post
    Route::get('apanel/add-services',[AccountController::class,'add_services']);//insert post
    Route::get('apanel/add-work',[AccountController::class,'add_work']);//insert post
    Route::post('apanel/post/insertpost',[AccountController::class,'insert_post']);//insert post
    Route::post('apanel/post/insertlogo',[AccountController::class,'insert_logo']);//insert post
    Route::post('apanel/product/insertproduct',[AccountController::class,'insert_product']);//insert post
    Route::post('apanel/product/insertnote',[AccountController::class,'insert_note']);//insert post
    Route::post('apanel/product/insertvacancy',[AccountController::class,'insert_vacancy']);//insert post
    Route::post('apanel/product/insertservices',[AccountController::class,'insert_services']);//insert post
    Route::post('apanel/post/insertposttext',[AccountController::class,'insert_post_text']);//insert post text
    Route::post('apanel/post/deleteposttext',[AccountController::class,'delete_post_text']);//remove post text
    Route::post('apanel/post/deleteposttextimage',[AccountController::class,'delete_post_text_image']);//remove post text image
    Route::post('apanel/post/updateposttext',[AccountController::class,'update_post_text']);//update post text
    Route::post('apanel/publishpost',[AccountController::class,'publishpost']);//from draft to production
    Route::post('apanel/publisharticle',[AccountController::class,'publish_article']);//from draft to production
    Route::post('apanel/post/deletepost',[AccountController::class,'delete_post']);
    Route::post('apanel/posts/updatepost',[AccountController::class,'update_post']);
    Route::post('apanel/service/updateservice',[AccountController::class,'update_service']);
    Route::post('apanel/product/updateproduct',[AccountController::class,'update_product']);
    Route::post('apanel/note/updatenote',[AccountController::class,'update_note']);
    Route::post('apanel/vacancy/updatevacancy',[AccountController::class,'update_vacancy']);
    Route::post('apanel/deletepostfromdrafts',[AccountController::class,'deletepostfromdrafts']);
    /*
     * Cover IMAGE manipulation
     */
    Route::get('apanel/coverimages',[AccountController::class,'cover_images']);
    Route::post('apanel/coverimages/uploadcoverimages',[AccountController::class,'upload_cover_images']);
    Route::post('apanel/coverimages/deletecoverimages',[AccountController::class,'delete_cover_images']);
//logout
    Route::get('logout', [AccountController::class,'logout'])->name('logout');
});
/*
{{SESSIONS}}
*/
Route::get('language/{any}', [MainController::class,'language'])->name('language');
