<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('post_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->float('price')->nullable();
            $table->text('text')->nullable();
            $table->text('description')->nullable();
            $table->text('meta_description')->nullable();
            $table->string('slug')->nullable();
            $table->integer('type_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->integer('lang_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
