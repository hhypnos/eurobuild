<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_nodes', function (Blueprint $table) {
            $table->id();
            $table->integer('file_id')->nullable();
            $table->integer('type_id')->nullable();
            $table->integer('data_id')->nullable();
            $table->string('relation_table')->nullable();
            $table->boolean('is_private')->default(false);
            $table->boolean('is_default')->default(false);
            $table->integer('order_id')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_nodes');
    }
}
