<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id')->default(0);
            $table->integer('uploaded_by')->nullable();
            $table->string('name');
            $table->string('name_original');
            $table->string('description')->nullable();
            $table->text('full_description')->nullable();
            $table->string('encoding')->nullable();
            $table->integer('size')->nullable();
            $table->string('metadata')->nullable();
            $table->string('mime_type')->nullable();
            $table->string('content_type')->nullable();
            $table->string('path')->nullable();
            $table->string('full_path')->nullable();
            $table->integer('source_id')->nullable();
            $table->integer('quality_id')->nullable();
            $table->integer('lang_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}
