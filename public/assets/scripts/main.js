function start() {
  anchors();
  aos();
  overlay();
  if (document.querySelector(".notification")) {
    notification();
  }
  if (document.querySelector(".lg")) {
    lg();
  }
  if (document.querySelector(".contact-page-form")) {
    contactForm();
  }
  if (document.querySelector(".product_inner-form")) {
    productInnerForm();
  }
  if (document.querySelector(".product_inner")) {
    productInner();
  }
  if (document.querySelector(".about-gallery-swiper")) {
    aboutGallerySwiper();
  }
  if (document.querySelector(".feature-swiper-container")) {
    featureSwiper();
  }
  if (document.querySelector(".vacancy")) {
    vacancy();
  }
  if (document.querySelector(".home-news-swiper-container")) {
    newsSwiper();
  }
  if (document.querySelector(".slider")) {
    sliderSwiper();
  }

  var srcs = document.querySelectorAll('*[data-src]');
  for (var i = 0; i < srcs.length; i++) {
    srcs[i].setAttribute('src', srcs[i].getAttribute('data-src'))
  }

  // document.querySelector(".header-content").style.maxWidth =
  //   "calc(100vw - " + getScrollbarWidth() + "px)";
}

window.onload = function () {
  document.querySelector("body").style.transition = "0.3s ease-in";
  document.querySelector("body").style.opacity = 1;
  document.querySelector(".overlay").style.display = "block";
  easeScroll();
  scrolls();
  start();
};
