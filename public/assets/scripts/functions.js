/**
 *  SETUP
 */

var _extends =
  Object.assign ||
  function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
function getScrollbarWidth() {
  // Creating invisible container
  const outer = document.createElement("div");
  outer.style.visibility = "hidden";
  outer.style.overflow = "scroll"; // forcing scrollbar to appear
  outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps
  document.body.appendChild(outer);

  // Creating inner element and placing it in the container
  const inner = document.createElement("div");
  outer.appendChild(inner);

  // Calculating difference between container's full width and the child width
  const scrollbarWidth = outer.offsetWidth - inner.offsetWidth;

  // Removing temporary elements from the DOM
  outer.parentNode.removeChild(outer);

  return scrollbarWidth;
}
var _resize;
function isEmail(value) {
  if (
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
      value
    )
  ) {
    return true;
  }
  return false;
}

function easeScroll(props) {
  scr(props);
}

function scrolls() {
  var html = document.querySelector("html");
  window.addEventListener("scroll", function () {
    if (window.scrollY > 0) {
      if (!html.classList.contains("scrolled")) {
        html.classList.add("scrolled");
      }
    } else {
      if (html.classList.contains("scrolled")) {
        html.classList.remove("scrolled");
      }
    }
  });
}

function resize(callback) {
  window.addEventListener("resize", function () {
    clearTimeout(_resize);
    _resize = setTimeout(function () {
      callback();
    }, 1000);
  });
}

function gridToggler() {
  document.addEventListener("keypress", function (event) {
    if (event.key === "g") {
      document.querySelector(".grid-preview").classList.toggle("active");
    }
  });
}

function anchors() {
    var anchors = document.querySelectorAll("a");
    for (var i = 0; i < anchors.length; i++) {
        anchors[i].addEventListener("click", function (event) {
            if (this.classList.contains('language_item')) {
                window.location.href = this.getAttribute('href');
            }
            if (
                !!this.getAttribute("href") &&
                this.getAttribute("href") === window.location.pathname
            ) {
                event.preventDefault();
            }
        });
    }
}

function aos() {
  AOS.init({
    once: true,
    offset: 50,
    duration: 800,
    ease: "ease-in-out",
  });
}

/**
 *  Functions
 */
function lg() {
  var items = document.querySelectorAll(".lg");
  for (var i = 0; i < items.length; i++) {
    lightGallery(items[i], {
      thumbnail: true,
    });
  }
}

function overlay() {
  document.querySelector(".header-bars").addEventListener("click", function () {
    document.querySelector("html").classList.toggle("has-overlay");
  });
  var items = document.querySelectorAll(".overlay-list-item.has-sub >a");
  for (var i = 0; i < items.length; i++) {
    items[i].addEventListener("click", function (event) {
      event.stopPropagation();
      event.preventDefault();
      slideToggle(event.target.nextElementSibling);
    });
  }
}

function productInnerForm() {
  var form = document.querySelector(".product_inner-form");
  var inputs = {
    phone: document.querySelector(
      'form.product_inner-form *[data-name="phone"]'
    ),
    address: document.querySelector(
      'form.product_inner-form *[data-name="address"]'
    ),
    message: document.querySelector(
      'form.product_inner-form *[data-name="message"]'
    ),
    delivery: document.querySelector(
      'form.product_inner-form *[data-name="delivery"]'
    ),
    load: document.querySelector('form.product_inner-form *[data-name="load"]'),
  };
  for (var item in inputs) {
    if (inputs[item]) {
      inputs[item].addEventListener("keyup", function () {
        this.classList.remove("invalid");
      });
    }
  }
  form.addEventListener("submit", function (event) {
    var values = {
      address: inputs.address && inputs.address.value,
      phone: inputs.phone && inputs.phone.value,
      delivery: inputs.delivery && inputs.delivery.checked,
      message: inputs.message && inputs.message.value,
      load: inputs.load && inputs.load.checked,
    };
    var valid = true;
    for (var item in values) {
      if (values[item]) {
        if (inputs[item].dataset.required) {
          if (!values[item]) {
            inputs[item].classList.add("invalid");
            valid = false;
          }
        }
      } else {
        if (inputs[item].dataset.required) {
          inputs[item].classList.add("invalid");
          valid = false;
        }
      }
    }
    if (!valid) {
      event.stopPropagation();
      event.preventDefault();
    }
  });
}

function contactForm() {
  var form = document.querySelector("form.contact-page-form");
  var inputs = {
    name: document.querySelector('form.contact-page-form *[data-name="name"]'),
    phone: document.querySelector(
      'form.contact-page-form *[data-name="phone"]'
    ),
    email: document.querySelector(
      'form.contact-page-form *[data-name="email"]'
    ),
    message: document.querySelector(
      'form.contact-page-form *[data-name="message"]'
    ),
  };
  for (var item in inputs) {
    if (inputs[item]) {
      inputs[item].addEventListener("keyup", function () {
        this.classList.remove("invalid");
      });
    }
  }
  form.addEventListener("submit", function (event) {
    var values = {
      name: inputs.name && inputs.name.value,
      phone: inputs.phone && inputs.phone.value,
      email: inputs.email && inputs.email.value,
      message: inputs.message && inputs.message.value,
    };
    var valid = true;
    for (var item in values) {
      if (values[item]) {
        if (inputs[item].dataset.required) {
          if (!values[item]) {
            inputs[item].classList.add("invalid");
            valid = false;
          }
        }
      } else {
        if (inputs[item].dataset.required) {
          inputs[item].classList.add("invalid");
          valid = false;
        }
      }
    }
    if (!valid) {
      event.stopPropagation();
      event.preventDefault();
    }
  });
}

function notification() {
  document
    .querySelector(".notification-close")
    .addEventListener("click", function () {
      document.querySelector(".notification").classList.remove("open");
    });
}

function aboutGallerySwiper() {
  var swiper = new Swiper(document.querySelector(".about-gallery-swiper"), {
    horizontal: true,
    slidesPerView: 3,
    spaceBetween: 25,
    breakpoints: {
      1025: {
        slidesPerView: 3,
        spaceBetween: 25,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      0: {
        slidesPerView: 1,
      },
    },
  });

  document
    .querySelector(".about-gallery-swiper-next")
    .addEventListener("click", function () {
      swiper.slideNext();
    });
  document
    .querySelector(".about-gallery-swiper-prev")
    .addEventListener("click", function () {
      swiper.slidePrev();
    });
}

function productInner() {
  var checkout = document.querySelector(".product_inner-info-button");
  if (checkout) {
    checkout.addEventListener("click", function () {
      this.style.display = "none";
      document.querySelector(
        ".product_inner-info-button_contact-wrap"
      ).style.display = "none";
      document.querySelector(".product_inner-form").classList.add("active");
    });
  }
}

function featureSwiper() {
  var swiper = new Swiper(document.querySelector(".feature-swiper-container"), {
    horizontal: true,
    slidesPerView: 3,
    spaceBetween: 25,
    breakpoints: {
      1025: {
        slidesPerView: 3,
        spaceBetween: 25,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      0: {
        slidesPerView: 1,
      },
    },
  });
  document
    .querySelector(".feature-swiper-navigation-next")
    .addEventListener("click", function () {
      swiper.slideNext();
    });
  document
    .querySelector(".feature-swiper-navigation-prev")
    .addEventListener("click", function () {
      swiper.slidePrev();
    });
}

function vacancy() {
  var items = document.querySelectorAll(".vacancy");
  for (let i = 0; i < items.length; i++) {
    items[i]
      .querySelector(".vacancy-button-btn")
      .addEventListener("click", function () {
        this.classList.toggle("active");
        slideToggle(items[i].querySelector(".vacancy-bot"));
      });
  }
}

function newsSwiper() {
  var swiper = new Swiper(
    document.querySelector(".home-news-swiper-container"),
    {
      horizontal: true,
      slidesPerView: 3,
      spaceBetween: 25,
      breakpoints: {
        1025: {
          slidesPerView: 3,
          spaceBetween: 25,
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 10,
        },
        0: {
          slidesPerView: 1,
        },
      },
    }
  );

  document
    .querySelector(".home-news-swiper-next")
    .addEventListener("click", function () {
      swiper.slideNext();
    });
  document
    .querySelector(".home-news-swiper-prev")
    .addEventListener("click", function () {
      swiper.slidePrev();
    });
}

function sliderSwiper() {
  var swiper = new Swiper(document.querySelector(".slider-swiper-container"), {
    slidesPerView: 1,
    spaceBetween: 0,
    // effect: "fade",
    // fadeEffect: {
    //   crossFade: true,
    // },
    pagination: {
      el: ".swiper-pagination",
      type: "bullets",
    },
  });
}
