function scr(e) {
  var t = {
      frameRate: 150,
      animationTime: 600,
      stepSize: 100,
      pulseAlgorithm: !0,
      pulseScale: 4,
      pulseNormalize: 1,
      accelerationDelta: 50,
      accelerationMax: 3,
      keyboardSupport: !0,
      arrowScroll: 50,
      fixedBackground: !0,
      excluded: '',
    },
    o = (t = _extends({}, t, e)),
    r = {
      left: 37,
      up: 38,
      right: 39,
      down: 40,
      spacebar: 32,
      pageup: 33,
      pagedown: 34,
      end: 35,
      home: 36,
    },
    n = { 37: 1, 38: 1, 39: 1, 40: 1 };
  function a() {
    if (!initDone && document.body) {
      initDone = !0;
      var e = document.body,
        t = document.documentElement,
        r = window.innerHeight,
        n = e.scrollHeight;
      if (
        ((root = document.compatMode.indexOf('CSS') >= 0 ? t : e),
        (activeElement = e),
        o.keyboardSupport && k('keydown', d),
        top != self)
      )
        isFrame = !0;
      else if (
        isOldSafari &&
        n > r &&
        (e.offsetHeight <= r || t.offsetHeight <= r)
      ) {
        var a,
          i = document.createElement('div');
        (i.style.cssText =
          'position:absolute; z-index:-10000; top:0; left:0; right:0; height:' +
          root.scrollHeight +
          'px'),
          document.body.appendChild(i),
          (refreshSize = function () {
            a ||
              (a = setTimeout(function () {
                isExcluded ||
                  ((i.style.height = '0'),
                  (i.style.height = root.scrollHeight + 'px'),
                  (a = null));
              }, 500));
          }),
          setTimeout(refreshSize, 10),
          k('resize', refreshSize);
        if (
          ((observer = new L(refreshSize)),
          observer.observe(e, {
            attributes: !0,
            childList: !0,
            characterData: !1,
          }),
          root.offsetHeight <= r)
        ) {
          var l = document.createElement('div');
          (l.style.clear = 'both'), e.appendChild(l);
        }
      }
      o.fixedBackground ||
        isExcluded ||
        ((e.style.backgroundAttachment = 'scroll'),
        (t.style.backgroundAttachment = 'scroll'));
    }
  }
  var i = [],
    l = !1,
    c = Date.now();
  function u(e, t, r) {
    var n, a;
    if (
      ((n = (n = t) > 0 ? 1 : -1),
      (a = (a = r) > 0 ? 1 : -1),
      (direction.x !== n || direction.y !== a) &&
        ((direction.x = n), (direction.y = a), (i = []), (c = 0)),
      1 != o.accelerationMax)
    ) {
      var u = Date.now() - c;
      if (u < o.accelerationDelta) {
        var s = (1 + 50 / u) / 2;
        s > 1 && ((s = Math.min(s, o.accelerationMax)), (t *= s), (r *= s));
      }
      c = Date.now();
    }
    if (
      (i.push({
        x: t,
        y: r,
        lastX: t < 0 ? 0.99 : -0.99,
        lastY: r < 0 ? 0.99 : -0.99,
        start: Date.now(),
      }),
      !l)
    ) {
      var d = F(),
        f = e === d || e === document.body;
      null == e.$scrollBehavior &&
        (function (e) {
          var t = w(e);
          if (null == b[t]) {
            var o = getComputedStyle(e, '')['scroll-behavior'];
            b[t] = 'smooth' == o;
          }
          return b[t];
        })(e) &&
        ((e.$scrollBehavior = e.style.scrollBehavior),
        (e.style.scrollBehavior = 'auto'));
      var m = function (n) {
        for (var a = Date.now(), c = 0, u = 0, s = 0; s < i.length; s++) {
          var d = i[s],
            h = a - d.start,
            w = h >= o.animationTime,
            v = w ? 1 : h / o.animationTime;
          o.pulseAlgorithm && (v = Y(v));
          var p = (d.x * v - d.lastX) >> 0,
            b = (d.y * v - d.lastY) >> 0;
          (c += p),
            (u += b),
            (d.lastX += p),
            (d.lastY += b),
            w && (i.splice(s, 1), s--);
        }
        f
          ? window.scrollBy(c, u)
          : (c && (e.scrollLeft += c), u && (e.scrollTop += u)),
          t || r || (i = []),
          i.length
            ? O(m, e, 1e3 / o.frameRate + 1)
            : ((l = !1),
              null != e.$scrollBehavior &&
                ((e.style.scrollBehavior = e.$scrollBehavior),
                (e.$scrollBehavior = null)));
      };
      O(m, e, 0), (l = !0);
    }
  }
  function s(e) {
    initDone || a();
    var t = e.target;
    if (e.defaultPrevented || e.ctrlKey) return !0;
    if (
      T(activeElement, 'embed') ||
      (T(t, 'embed') && /\.pdf/i.test(t.src)) ||
      T(activeElement, 'object') ||
      t.shadowRoot
    )
      return !0;
    var r = -e.wheelDeltaX || e.deltaX || 0,
      n = -e.wheelDeltaY || e.deltaY || 0;
    isMac &&
      (e.wheelDeltaX &&
        z(e.wheelDeltaX, 120) &&
        (r = (e.wheelDeltaX / Math.abs(e.wheelDeltaX)) * -120),
      e.wheelDeltaY &&
        z(e.wheelDeltaY, 120) &&
        (n = (e.wheelDeltaY / Math.abs(e.wheelDeltaY)) * -120)),
      r || n || (n = -e.wheelDelta || 0),
      1 === e.deltaMode && ((r *= 40), (n *= 40));
    var i = x(t);
    return i
      ? !!(function (e) {
          if (!e) return;
          deltaBuffer.length || (deltaBuffer = [e, e, e]);
          (e = Math.abs(e)),
            deltaBuffer.push(e),
            deltaBuffer.shift(),
            clearTimeout(deltaBufferTimer),
            (deltaBufferTimer = setTimeout(function () {
              try {
                localStorage.SS_deltaBuffer = deltaBuffer.join(',');
              } catch (e) {}
            }, 1e3));
          var t = e > 120 && C(e);
          return !C(120) && !C(100) && !t;
        })(n) ||
          (Math.abs(r) > 1.2 && (r *= o.stepSize / 120),
          Math.abs(n) > 1.2 && (n *= o.stepSize / 120),
          u(i, r, n),
          e.preventDefault(),
          void y())
      : !isFrame ||
          !isChrome ||
          (Object.defineProperty(e, 'target', { value: window.frameElement }),
          parent.wheel(e));
  }
  function d(e) {
    var t = e.target,
      a =
        e.ctrlKey ||
        e.altKey ||
        e.metaKey ||
        (e.shiftKey && e.keyCode !== r.spacebar);
    document.body.contains(activeElement) ||
      (activeElement = document.activeElement);
    var i = /^(button|submit|radio|checkbox|file|color|image)$/i;
    if (
      e.defaultPrevented ||
      /^(textarea|select|embed|object)$/i.test(t.nodeName) ||
      (T(t, 'input') && !i.test(t.type)) ||
      T(activeElement, 'video') ||
      (function (e) {
        var t = e.target,
          o = !1;
        if (-1 != document.URL.indexOf('www.youtube.com/watch'))
          do {
            if (
              (o = t.classList && t.classList.contains('html5-video-controls'))
            )
              break;
          } while ((t = t.parentNode));
        return o;
      })(e) ||
      t.isContentEditable ||
      a
    )
      return !0;
    if (
      (T(t, 'button') || (T(t, 'input') && i.test(t.type))) &&
      e.keyCode === r.spacebar
    )
      return !0;
    if (T(t, 'input') && 'radio' == t.type && n[e.keyCode]) return !0;
    var l = 0,
      c = 0,
      s = x(activeElement);
    if (!s) return !isFrame || !isChrome || parent.keydown(e);
    var d = s.clientHeight;
    switch ((s == document.body && (d = window.innerHeight), e.keyCode)) {
      case r.up:
        c = -o.arrowScroll;
        break;
      case r.down:
        c = o.arrowScroll;
        break;
      case r.spacebar:
        c = -(e.shiftKey ? 1 : -1) * d * 0.9;
        break;
      case r.pageup:
        c = 0.9 * -d;
        break;
      case r.pagedown:
        c = 0.9 * d;
        break;
      case r.home:
        s == document.body &&
          document.scrollingElement &&
          (s = document.scrollingElement),
          (c = -s.scrollTop);
        break;
      case r.end:
        var f = s.scrollHeight - s.scrollTop - d;
        c = f > 0 ? f + 10 : 0;
        break;
      case r.left:
        l = -o.arrowScroll;
        break;
      case r.right:
        l = o.arrowScroll;
        break;
      default:
        return !0;
    }
    u(s, l, c), e.preventDefault(), y();
  }
  function f(e) {
    activeElement = e.target;
  }
  var m,
    h,
    w =
      ((m = 0),
      function (e) {
        return e.uniqueID || (e.uniqueID = m++);
      }),
    v = {},
    p = {},
    b = {};
  function y() {
    clearTimeout(h),
      (h = setInterval(function () {
        v = p = b = {};
      }, 1e3));
  }
  function g(e, t, o) {
    for (var r = o ? v : p, n = e.length; n--; ) r[w(e[n])] = t;
    return t;
  }
  function S(e, t) {
    return (t ? v : p)[w(e)];
  }
  function x(e) {
    var t = [],
      o = document.body,
      r = root.scrollHeight;
    do {
      var n = S(e, !1);
      if (n) return g(t, n);
      if ((t.push(e), r === e.scrollHeight)) {
        var a = (E(root) && E(o)) || D(root);
        if ((isFrame && B(root)) || (!isFrame && a)) return g(t, F());
      } else if (B(e) && D(e)) return g(t, e);
    } while ((e = e.parentElement));
  }
  function B(e) {
    return e.clientHeight + 10 < e.scrollHeight;
  }
  function E(e) {
    return 'hidden' !== getComputedStyle(e, '').getPropertyValue('overflow-y');
  }
  function D(e) {
    var t = getComputedStyle(e, '').getPropertyValue('overflow-y');
    return 'scroll' === t || 'auto' === t;
  }
  function k(e, t, o) {
    window.addEventListener(e, t, o || !1);
  }
  function M(e, t, o) {
    window.removeEventListener(e, t, o || !1);
  }
  function T(e, t) {
    return e && (e.nodeName || '').toLowerCase() === t.toLowerCase();
  }
  if (window.localStorage && localStorage.SS_deltaBuffer)
    try {
      deltaBuffer = localStorage.SS_deltaBuffer.split(',');
    } catch (e) {}
  function z(e, t) {
    return Math.floor(e / t) == e / t;
  }
  function C(e) {
    return z(deltaBuffer[0], e) && z(deltaBuffer[1], e) && z(deltaBuffer[2], e);
  }
  var H,
    O =
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      function (e, t, o) {
        window.setTimeout(e, o || 1e3 / 60);
      },
    L =
      window.MutationObserver ||
      window.WebKitMutationObserver ||
      window.MozMutationObserver,
    F =
      ((H = document.scrollingElement),
      function () {
        if (!H) {
          var e = document.createElement('div');
          (e.style.cssText = 'height:10000px;width:1px;'),
            document.body.appendChild(e);
          var t = document.body.scrollTop;
          document.documentElement.scrollTop,
            window.scrollBy(0, 3),
            (H =
              document.body.scrollTop != t
                ? document.body
                : document.documentElement),
            window.scrollBy(0, -3),
            document.body.removeChild(e);
        }
        return H;
      });
  function X(e) {
    var t, r;
    return (
      (e *= o.pulseScale) < 1
        ? (t = e - (1 - Math.exp(-e)))
        : ((e -= 1), (t = (r = Math.exp(-1)) + (1 - Math.exp(-e)) * (1 - r))),
      t * o.pulseNormalize
    );
  }
  function Y(e) {
    return e >= 1
      ? 1
      : e <= 0
      ? 0
      : (1 == o.pulseNormalize && (o.pulseNormalize /= X(1)), X(e));
  }
  var A = !1;
  try {
    window.addEventListener(
      'test',
      null,
      Object.defineProperty({}, 'passive', {
        get: function () {
          A = !0;
        },
      })
    );
  } catch (e) {}
  var K = !!A && { passive: !1 },
    N = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';
  function P(e) {
    for (var r in e) t.hasOwnProperty(r) && (o[r] = e[r]);
  }
  N && isEnabledForBrowser && (k(N, s, K), k('mousedown', f), k('load', a)),
    (P.destroy = function () {
      observer && observer.disconnect(),
        M(N, s),
        M('mousedown', f),
        M('keydown', d),
        M('resize', refreshSize),
        M('load', a);
    }),
    window.SmoothScrollOptions && P(window.SmoothScrollOptions),
    'function' == typeof define && define.amd
      ? define(function () {
          return P;
        })
      : 'object' == typeof exports
      ? (module.exports = P)
      : (window.SmoothScroll = P);
}
