<div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
    <aside class="sidebar">
        <div class="bg-layer">
            <div class="image-layer" style="background-image:url();"></div>
        </div>

        <div class="sidebar-widget search-box">
            <div class="widget-inner">
                <div class="sidebar-title">
                    <h4>Search</h4>
                </div>
                <form method="get" action="{{url()->current()}}">
                    <div class="form-group">
                        <input type="search" name="q" value="" placeholder="Keyword..." required="">
                        <button type="submit"><span class="icon flaticon-magnifying-glass"></span></button>
                    </div>
                </form>
            </div>
        </div>

        <div class="sidebar-widget cat-widget">
            <div class="widget-content">
                <div class="sidebar-title">
                    <h4>{!! translate($static_contents("sidebar","category_title")->content,session('languageID')) !!}</h4>
                </div>
                <ul class="cat-links clearfix">
                    <li @if(!request('c')) class="current" @endif><a href="{{url()->current()}}">{!! translate($static_contents("sidebar","category_all")->content,session('languageID')) !!}</a></li>
                    @foreach($categories as $category)
                    <li @if(request('c') == $category->id) class="current" @endif><a href="{{URL::to(last(request()->segments()).'?c='.$category->id)}}">{{translate($category->category,session('languageID'))}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <!--Posts-->
        <div class="sidebar-widget recent-posts">
<!--            <div class="widget-inner">

                <div class="recent-posts-box">
                    <div class="post">
                        <div class="inner">
                            <figure class="post-thumb"><img src="{!! URL::to(translate($static_contents(last(request()->segments()),"banner-third")['content'],session('languageID'))) !!}" alt=""><a href="#" class="overlink"><span class="icon flaticon-zoom-in"></span></a></figure>
                        </div>
                    </div>

                    <div class="post">
                        <div class="inner">
                            <figure class="post-thumb"><img src="{!! URL::to(translate($static_contents(last(request()->segments()),"banner-fourth")['content'],session('languageID'))) !!}" alt=""></figure>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>

    </aside>
</div>
