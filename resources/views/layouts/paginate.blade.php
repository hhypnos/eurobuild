@if ($paginator->lastPage() > 1)

    <div class="row">
        <div class="col-xxs-12">
            <div class="news-pagination">
                <div
                    class="pagination-wrapper"
                    data-aos="fade-up"
                    data-aos-delay="500"
                >
                    <ul class="pagination">
                        <li>
                            <a href="{{ $paginator->url($paginator->currentPage()-1) }}"><span class="fa fa-angle-left"></span></a>
                        </li>
                        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                            <li>
                                <a href="{{ $paginator->url($i) }}" class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">{{$i}}</a>
                            </li>
                        @endfor
                        <li>
                            <a href="{{($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}"
                            ><span class="fa fa-angle-right"></span
                                ></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endif
