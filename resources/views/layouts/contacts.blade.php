<div class="contact" data-aos="fade-in">
    <div class="container-wrapper">
        <div class="container-xxs">
            <div class="row contact-row">
                <div class="col-xxs-12 offset-xxs-3 contact-bg"></div>
                <div class="col-xl-5 offset-xl-1 col-md-6 col-xxs-12">
                    <div
                        class="contact-left"
                        data-aos="fade-in"
                        data-aos-delay="300"
                        style="z-index: 10"
                    >
                        <img src="{{URL::to('assets/images/contact.png')}}" />
                    </div>
                </div>
                <div class="col-xl-4 offset-xl-1 col-md-6 col-xxs-12">
                    <div class="contact-right">
                        <div class="contact-info">
                            <h2
                                class="contact-title"
                                data-aos="fade-up"
                                data-aos-delay="500"
                            >
                                {!! translate($static_contents("all","title_1")->content,session('languageID')) !!}
                            </h2>
                            <div
                                class="contact-address"
                                data-aos="fade-up"
                                data-aos-delay="500"
                            >
                                {!! translate($static_contents("all","title_1_text")->content,session('languageID')) !!}

                            </div>
                            <div
                                class="contact-hotline"
                                data-aos="fade-up"
                                data-aos-delay="500"
                            >
                                <a href="tel:0322524343">{!! translate($static_contents("all","title_1_text2")->content,session('languageID')) !!}</a>
                            </div>
                            <div
                                class="contact-mail"
                                data-aos="fade-up"
                                data-aos-delay="500"
                            >
                                <a href="mailto:contact@weve.tech"></a>
                            </div>
                            <div
                                class="contact-map"
                                data-aos="fade-up"
                                data-aos-delay="500"
                            >
                                <a
                                    href="https://www.google.com/maps/place/%E1%83%95%E1%83%90%E1%83%96%E1%83%98%E1%83%A1%E1%83%A3%E1%83%91%E1%83%9C%E1%83%98%E1%83%A1+I+%E1%83%9B%2F%E1%83%A0+%237/@41.6940032,44.8495616,14z/data=!4m5!3m4!1s0x40440da167a9d50f:0xdc293f8f5c4423e0!8m2!3d41.7000715!4d44.8497421"
                                    target="_BLANK"
                                >გვიპოვე რუკაზე</a
                                >
                            </div>
                            <div
                                class="contact-networks"
                                data-aos="fade-up"
                                data-aos-delay="500"
                            >
                                <div class="contact-networks-title">
                                    შემოგვიერთდით:
                                </div>
                                {!! translate($static_contents("all","title_1_social")->content,session('languageID')) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
