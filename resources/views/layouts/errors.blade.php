@if (count($errors))
<div class="form-group ">
    <div class="alert alert-danger">
    <ul>

    @foreach ($errors->all() as $error)

        <li>{{ translate($error,session("languageID")) }}</li>

    @endforeach

    </ul>
    </div>
</div>
@endif
