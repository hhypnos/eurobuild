<footer class="footer" data-aos="fade-down">
    <div class="container-wrapper">
        <div class="container-xxs">
            <div class="row">
                <div
                    class="col-xl-8 col-lg-10 offset-lg-1 offset-xl-2 col-xxs-12"
                >
                    <div class="footer-content">
                        <div
                            class="footer-content-block footer-content-block-first"
                        >
                            <div class="footer-content-block-item">
                                <div class="footer-logo">
                                    <a href="{{URL::to('/')}}"
                                    ><img
                                            src="{{$static_contents("all","footer-logo")->content}}"
                                            width="100%"
                                        /></a>
                                </div>
                                <div class="footer-copy">copyright: wevetech 2021</div>
                                <div class="footer-networks">
                                    <div class="network">
                                        <div class="network-item">
                                            <a href="https://facebook.com"
                                            ><span class="fa fa-facebook"> </span
                                                ></a>
                                        </div>
                                        <div class="network-item">
                                            <a href="https://google.com"
                                            ><span class="fa fa-google"> </span
                                                ></a>
                                        </div>
                                        <div class="network-item">
                                            <a href="https://instagram.com"
                                            ><span class="fa fa-instagram"></span
                                                ></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer-hotline">
                                    <a href="tel:{!! translate($static_contents("all","hot_line_number")->content,session('languageID')) !!}">{!! translate($static_contents("all","hot_line")->content,session('languageID')) !!}</a>
                                </div>
                            </div>
                        </div>
                        <div class="footer-content-block">
                            <div class="footer-content-block-item">
                                <div class="footer-content-block-item-title">
                                    {!! translate($static_contents("all","footer_catalog")->content,session('languageID')) !!}
                                </div>
                                <ul class="footer-content-block-item-list">
                                    @foreach($navigations as $navigation)
                                        @if(count($navigation->children)>0)
                                                @foreach($navigation->children as $child)
                                                    <li  class="footer-content-block-item-list-item"><a href="{{URL::to($child->slug)}}">{{$child->title}}</a></li>
                                                @endforeach
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="footer-content-block">
                            <div class="footer-content-block-item">
                                <div class="footer-content-block-item-title">
                                    {!! translate($static_contents("all","footer_services")->content,session('languageID')) !!}
                                </div>
                                <ul class="footer-content-block-item-list">
                                    @foreach($services as $service)
                                    <li class="footer-content-block-item-list-item">
                                        <a href="{{\URL::to('about')}}">{{$service->title}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="footer-content-block">
                            <div class="footer-content-block-item">
                                <div class="footer-content-block-item-title">
                                    {!! translate($static_contents("all","footer_pages")->content,session('languageID')) !!}
                                </div>
                                <ul class="footer-content-block-item-list">
                                    @foreach($navigations as $navigation)
                                    <li class="footer-content-block-item-list-item">
                                        <a href="{{URL::to($navigation->slug)}}">{{$navigation->title}}</a>
                                    </li>
                                  @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
</div>
</div>

<style>
.header-logo >a {
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    background: none !important;
}
.header-logo >a img {
  background: none !important;
}
.footer-logo {background: none}
</style>
<script src="https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/js/lightgallery.js"></script>
<script src="https://cdn.rawgit.com/sachinchoolur/lg-pager.js/master/dist/lg-pager.js"></script>
<script src="https://cdn.rawgit.com/sachinchoolur/lg-autoplay.js/master/dist/lg-autoplay.js"></script>
<script src="https://cdn.rawgit.com/sachinchoolur/lg-fullscreen.js/master/dist/lg-fullscreen.js"></script>
<script src="https://cdn.rawgit.com/sachinchoolur/lg-share.js/master/dist/lg-share.js"></script>
<script src="https://cdn.rawgit.com/sachinchoolur/lg-zoom.js/master/dist/lg-zoom.js"></script>
<script src="https://cdn.rawgit.com/sachinchoolur/lg-hash.js/master/dist/lg-hash.js"></script>
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="{{URL::to('assets/scripts/aos.js')}}"></script>
<script src="{{URL::to('assets/scripts/swiper-bundle.min.js')}}"></script>
<script src="{{URL::to('assets/scripts/barba.js')}}"></script>
<script src="{{URL::to('assets/scripts/stats.js')}}"></script>
<script src="{{URL::to('assets/scripts/browsers.js')}}"></script>
<script src="{{URL::to('assets/scripts/scr.js')}}"></script>
<script src="{{URL::to('assets/scripts/slideToggle.js')}}"></script>
<script src="{{URL::to('assets/scripts/functions.js')}}"></script>
<script src="{{URL::to('assets/scripts/main.js')}}"></script>
</body>
</html>
