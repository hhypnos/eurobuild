<!DOCTYPE html>
<html lang="en" style=" width: 100vw;">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-9ZVV8VBZ5Q"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-9ZVV8VBZ5Q');
</script>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
        rel="stylesheet"
        href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
    />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,400;0,500;0,700;1,400&amp;display=swap"
        rel="stylesheet"
    />
    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.10.0/css/lightgallery.min.css"
    />
    <link rel="stylesheet" href="{{URL::to('assets/styles/main.css')}}" />
    <link
        rel="stylesheet"
        href="https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/css/lightgallery.css"
    />
    <link
        rel="icon"
        href="{{URL::to('favicon.ico')}}"
        type="image/*"
        sizes="16x16"
    />
    <title>Ebuild.ge || ევრობილდი</title>
    <script src="https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js "></script>
    <script>
        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
<!-- Messenger Chat plugin Code -->
    <div id="fb-root"></div>

    <!-- Your Chat plugin code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>

    <script>
      var chatbox = document.getElementById('fb-customer-chat');
      chatbox.setAttribute("page_id", "1399562873698860");
      chatbox.setAttribute("attribution", "biz_inbox");

      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v11.0'
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>
</head>

<body style="opacity: 0">
<div id="fb-root"></div>
<script
    async=""
    defer=""
    crossorigin="anonymous"
    src="https://connect.facebook.net/ka_GE/sdk.js#xfbml=1&amp;version=v10.0&amp;appId=516891232589876&amp;autoLogAppEvents=1"
    nonce="LF3MDwAM"
></script>
<div class="grid-preview">
    <div class="container-wrapper">
        <div class="container-xxs">
            <div class="row">
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
                <div class="col-xxs-1">
                    <div class="grid-preview-content"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="root-wrapper" data-barba="wrapper">
    <div class="root" data-barba="container">
        <div class="overlay" style="display: none">
            <div class="container-wrapper">
                <div class="container-xxs">
                    <div class="row">
                        <div class="col-xxs-12">
                            <div class="overlay-content">
                                <ul class="overlay-list">
                                    @foreach($navigations as $navigation)
                                        @php($fetchSlug = explode('/', url()->full()))
                                        @php($slugClass = '')
                                        @if ($loop->first && URL::current()==ENV('APP_URL'))
                                            @php($slugClass = 'active')
                                        @endif
                                        @if($navigation->slug == $fetchSlug[(count($fetchSlug) - 1)])
                                            @php($slugClass = 'active')
                                        @endif
                                        <li class="overlay-list-item {{$slugClass}} {{count($navigation->children)>0?'has-sub':''}}"><a href="{{URL::to($navigation->slug)}}">{{$navigation->title}}</a>
                                            @if(count($navigation->children)>0)
                                                <ul class="overlay-sub-list">
                                                    @foreach($navigation->children as $child)
                                                        <li  class="overlay-sub-list-item"><a href="{{URL::to($child->slug)}}">{{$child->title}}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>
@if(request()->segment(count(request()->segments())) =="contact" || request()->segment(count(request()->segments())-1) =="products")
<img class="header-image" src="{{!empty($page_cover) ? $page_cover->content : ''}}" />

@endif
        <header class="header {{request()->segment(count(request()->segments())) =="contact" || request()->segment(count(request()->segments())-1) =="products" ? 'header-bg': 'header-shrink'}}">
            <div class="container-wrapper">
                <div class="container-xxs">
                    <div class="row">
                        <div class="col-xxs-12">
                            <div class="header-content">
                                <div class="header-logo">
                                    <a href="{{URL::to('/')}}"
                                    ><img
                                            src="{{$static_contents("all","header-logo")->content}}"
                                        /></a>
                                </div>
                                <div class="header-mobile">
                                    <div class="header-language-switch">
                                        <div class="language-switch">
                                            <div class="language-switch-current">
                                                <div class="language-switch-current-flag">
                                                    <!-- <img src="{{URL::to('assets/images/'.session('languageID').'.svg')}}" /> -->
                                                </div>
                                                @if(session('languageID') ==1)
                                                <div class="language-switch-current-name">ქართ</div>
                                                @endif
                                                @if(session('languageID') ==2)
                                                    <div class="language-switch-current-name">ENG</div>
                                                @endif
                                                @if(session('languageID') ==3)
                                                    <div class="language-switch-current-name">РУС</div>
                                                @endif
                                                <div class="language-switch-arrow">
                                                    <span class="fa fa-angle-down"></span>
                                                </div>
                                            </div>
                                            <ul class="language-switch-items">
                                                <li class="language-switch-item">
                                                    <a href="{{URL::to('/')}}/language/1" class="language_item">ქართული</a>
                                                </li>
                                                <li class="language-switch-item">
                                                    <a href="{{URL::to('/')}}/language/2" class="language_item">English</a>
                                                </li>
                                                <li class="language-switch-item">
                                                    <a href="{{URL::to('/')}}/language/3" class="language_item">Руский</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <button class="header-bars">
                        <span class="fa fa-bars"> </span
                        ><span class="fa fa-times"></span>
                                    </button>
                                </div>
                                <div class="header-menu">
                                    <ul class="header-list">
                                        @foreach($navigations as $navigation)
                                            @php($fetchSlug = explode('/', url()->full()))
                                            @php($slugClass = '')
                                            @if ($loop->first && URL::current()==ENV('APP_URL'))
                                                @php($slugClass = 'active')
                                            @endif
                                            @if($navigation->slug == $fetchSlug[(count($fetchSlug) - 1)])
                                                @php($slugClass = 'active')
                                            @endif
                                            <li class="header-list-item {{$slugClass}} {{count($navigation->children)>0?'has-sub':''}}"><a href="{{URL::to($navigation->slug)}}">{{$navigation->title}}</a>
                                                @if(count($navigation->children)>0)
                                                    <ul class="header-sub-list">
                                                        @foreach($navigation->children as $child)
                                                            <li  class="header-sub-list-item"><a href="{{$child->slug}}">{{$child->title}}</a></li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach

                                    </ul>
                                    <div class="header-language-switch">
                                        <div class="language-switch">
                                            <div class="language-switch-current">
                                                <div class="language-switch-current-flag">
                                                    <img src="{{URL::to('assets/images/'.session('languageID').'.svg')}}" />
                                                </div>
                                                @if(session('languageID') ==1)
                                                    <div class="language-switch-current-name">ქართ</div>
                                                @endif
                                                @if(session('languageID') ==2)
                                                    <div class="language-switch-current-name">ENG</div>
                                                @endif
                                                @if(session('languageID') ==3)
                                                    <div class="language-switch-current-name">РУС</div>
                                                @endif
                                                <div class="language-switch-arrow">
                                                    <span class="fa fa-angle-down"></span>
                                                </div>
                                            </div>
                                            <ul class="language-switch-items">
                                                    <li class="language-switch-item">
                                                        <a href="{{URL::to('/')}}/language/1" class="language_item">ქართული</a>
                                                    </li>
                                                <li class="language-switch-item">
                                                    <a href="{{URL::to('/')}}/language/2" class="language_item">English</a>
                                                </li>
                                                <li class="language-switch-item">
                                                    <a href="{{URL::to('/')}}/language/3" class="language_item">Руский</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
