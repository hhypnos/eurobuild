@extends('layouts.master')
@section('content')
    <div class="background" data-aos="fade-in">
        @php
            $check_format = !empty($page_cover) ? explode(".",$page_cover->content)[count(explode(".",$page_cover->content))-1] : null;
        @endphp
        @if(!empty($page_cover))
            @if((strtolower($check_format) == 'mp4' || strtolower($check_format) == 'avi' || strtolower($check_format) == 'ogg' || strtolower($check_format) == 'webm'))
                <video
                    class="background-video"
                    poster="{{!empty($page_cover) ? $page_cover->content : ''}}"
                    playsinline="playsinline"
                    autoplay="autoplay"
                    muted="muted"
                    loop="loop"
                    data-src="{{!empty($page_cover) ? $page_cover->content : ''}}"
                ></video>
            @else
                <img
                    class="background-image"
                    src="{{!empty($page_cover) ? $page_cover->content : ''}}"
                    data-aos="fade-in"
                    data-aos-delay="200"
                />
            @endif
        @endif
        <div
            class="background-title"
            data-aos="fade-down"
            data-aos-delay="300"
        >
            {!! translate($static_contents("gallery","title")->content,session('languageID')) !!}
        </div>
    </div>
            <div class="gallery">
                <div class="container-wrapper">
                    <div class="container-xxs">
                        <div class="row">
                            <div
                                class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-xxs-12"
                            >
                                <div
                                    class="gallery-content"
                                    data-aos="fade-in"
                                    data-aos-delay="100"
                                >
                                    <div class="gallery-content-list lg">
                                        @foreach($galleries as $gallery)
                                            <div
                                                class="gallery_item-wrap"
                                                data-aos="fade-up"
                                                data-aos-delay="500"
                                                data-src="{{$gallery->image}}"
                                            >
                                                <div class="gallery_item">
                                                    <a class="gallery_item_link lg-item"
                                                    ><img src="{{$gallery->image}}" alt="{{$gallery->alt}}" /><span
                                                            class="fa fa-search gallery_item-zoom"
                                                        ></span
                                                        ></a>
                                                </div>
                                            </div>
                                            @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           @include('layouts.contacts')
@endsection
