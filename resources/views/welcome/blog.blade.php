@extends('layouts.master')
@section('content')

    <!--Breadcrumbs-->
    @include("layouts.breadcrumbs")

    <!--Welcome Section-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="content-inner">
                        <div class="blog-posts">
                                @foreach($posts as $post)
                                <!--News Block-->
                                <div class="news-block-four">
                                    <div class="inner-box">
                                        <div class="image-box">
                                            <figure class="image"><img src="{{$post->image}}" alt="{{$post->title}}"></figure>
                                            <div class="date"><span>{{$post->date}}</span></div>
                                            <div class="hover-box">
                                                <div class="more-link"><a href="{{URL::to('post/'.$post->slug)}}">Read More</a></div>
                                            </div>
                                        </div>
                                        <div class="lower-box">
                                            <div class="upper-info">
                                                <h4><a href="{{URL::to('post/'.$post->slug)}}">{{$post->title}}</a></h4>
                                            </div>

                                            <div class="meta-info clearfix">
                                                <div class="author-info clearfix">
<!--                                                    <div class="author-icon"><span class="flaticon-user-3"></span></div>-->

                                                </div>
                                                <div class="comments-info" style="position: absolute;top: -125%;right: 0;">
                                                   <!-- <a href="#"><span class="fa fa-eye"></span> {{$post->views}}</a>-->
                                                </div>
                                                <div class="share-it">
                                                    <a class="share-btn" href="#"><span class="icon flaticon-share"></span></a>
                                                    <div class="share-list">
                                                        <ul>
                                                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{URL::to('post/'.$post->slug)}}"
                                                                   target="popup"
                                                                   onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{URL::to('post/'.$post->slug)}}','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"><span class="fab fa-facebook-f"></span></a></li>
                                                            <li><a href="https://twitter.com/intent/tweet?url={{URL::to('post/'.$post->slug)}}&text={{$post->title}}"
                                                                   target="popup"
                                                                   onclick="window.open('https://twitter.com/intent/tweet?url={{URL::to('post/'.$post->slug)}}&text={{$post->title}}','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"><span class="fab fa-twitter"></span></a></li>
                                                            <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{URL::to('post/'.$post->slug)}}&title={{$post->title}}"
                                                                   target="popup"
                                                                   onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url={{URL::to('post/'.$post->slug)}}&title={{$post->title}}','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"><span class="fab fa-linkedin-in"></span></a></li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                        </div>
                        <!--Pagination-->
                        @include('layouts.paginate', ['paginator' => $posts])
                    </div>
                </div>
                @include('layouts.sidebar')
            </div>
        </div>
    </div>
@endsection
