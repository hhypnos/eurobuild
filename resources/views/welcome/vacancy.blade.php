@extends('layouts.master')
@section('content')
    <div class="background" data-aos="fade-in">
        @php
            $check_format = !empty($page_cover) ? explode(".",$page_cover->content)[count(explode(".",$page_cover->content))-1] : null;
        @endphp
        @if(!empty($page_cover))
            @if((strtolower($check_format) == 'mp4' || strtolower($check_format) == 'avi' || strtolower($check_format) == 'ogg' || strtolower($check_format) == 'webm'))
                <video
                    class="background-video"
                    poster="{{!empty($page_cover) ? $page_cover->content : ''}}"
                    playsinline="playsinline"
                    autoplay="autoplay"
                    muted="muted"
                    loop="loop"
                    data-src="{{!empty($page_cover) ? $page_cover->content : ''}}"
                ></video>
            @else
                <img
                    class="background-image"
                    src="{{!empty($page_cover) ? $page_cover->content : ''}}"
                    data-aos="fade-in"
                    data-aos-delay="200"
                />
            @endif
        @endif
        <div
            class="background-title"
            data-aos="fade-down"
            data-aos-delay="300"
        >
            {!! translate($static_contents("vacancy","title")->content,session('languageID')) !!}
        </div>
    </div>
    <div class="vacnacies page">
        <div class="vacancy-wrapper">
            <div class="container-wrapper">
                <div class="container-xxs">
                    <div class="row">
                        <div class="col-xxl-6 offset-xxl-1 col-lg-8 col-xxs-12">
                            <div class="vacancies-header">
                                <div class="block_title" data-aos="fade-up">
                                    <div class="block_title-text">{!! translate($static_contents("vacancy","title")->content,session('languageID')) !!}</div>
                                </div>
                            </div>
                            <div class="vacancies-list">
                                @foreach($vacancies as $vacancy)
                                    <div class="vacancies-list-item" data-aos="fade-up">
                                        <div class="vacancy">
                                            <div class="vacancy-top">
                                                <div class="vacancy-title">
                                                    {{$vacancy->title}}
                                                </div>
                                                <div class="vacancy-right">
                                                    <div class="vacancy-top-item">
                                                        <div class="vacancy-top-item-content">
                                                            <div class="vacnacy-top-item-title">
                                                                {!! translate($static_contents("vacancy","published")->content,session('languageID')) !!}
                                                            </div>
                                                            <div class="vacancy-top-item-value">
                                                                {{date('Y-m-d',strtotime($vacancy->created_at))}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="vacancy-top-item">
                                                        <div class="vacancy-top-item-content">
                                                            <div class="vacnacy-top-item-title">
                                                                {!! translate($static_contents("vacancy","exp_date")->content,session('languageID')) !!}
                                                            </div>
                                                            <div class="vacancy-top-item-value">
                                                                {{$vacancy->expiration_date}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="vacancy-button">
                                                        <button class="vacancy-button-btn">
                                                            {!! translate($static_contents("vacancy","details")->content,session('languageID')) !!}<span
                                                                class="fa fa-angle-down"
                                                            ></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vacancy-bot">
                                                {!!  $vacancy->description !!}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-xxl-5 col-lg-4 col-xxs-12">
                            {!! $vacancies[0]->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            @include('layouts.contacts')
@endsection
