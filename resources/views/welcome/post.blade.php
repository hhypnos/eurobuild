@extends('layouts.master')
@section('content')

    <section class="blog-banner">
        <div class="image-layer" style="background-image:url({{$post->cover}});"></div>

        <div class="banner-inner">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <div class="meta-info clearfix">

                    </div>
                    <h2>{{$post->title}}</h2>
                    <div class="author-info">
                    </div>
                    <div class="other-info clearfix">
                        <div class="date"><span>{{$post->date}}</span></div>
                        <div class="tags">
                            @foreach($post->categories as $category)
                            <a href="#">{{translate($category->full_category->category,session('languageID'))}}</a>
                                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Welcome Section-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Content Side-->
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="content-inner">
                        <div class="single-post">
                            <div class="post-details" style="text-align: justify;">
                                <div class="main-image-box">
                                    <figure class="image"><img src="{{$post->image}}" alt=""></figure>
                                </div>
                                <p>{!! $post->text !!}</p>

                            </div>

                            <div class="share-post">
                                <strong>{!! translate($static_contents("post","share_btns")->content,session('languageID')) !!}</strong>
                                <ul class="links clearfix">
                                    <li class="facebook">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}"
                                           target="popup"
                                           onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"><span class="icon fab fa-facebook-f"></span><span class="txt">Facebook</span></a></li>
                                    <li class="twitter"><a href="https://twitter.com/intent/tweet?url={{Request::fullUrl()}}&text={{$post->title}}"
                                                           target="popup"
                                                           onclick="window.open('https://twitter.com/intent/tweet?url={{Request::fullUrl()}}&text={{$post->title}}','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"><span class="icon fab fa-twitter"></span><span class="txt">Twiter</span></a></li>
                                    <li class="linkedin"><a href="https://www.linkedin.com/shareArticle?mini=true&url={{Request::fullUrl()}}&title={{$post->title}}"
                                                            target="popup"
                                                            onclick="window.open('https://www.linkedin.com/shareArticle?mini=true&url={{Request::fullUrl()}}&title={{$post->title}}','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;"><span class="icon fab fa-linkedin-in"></span><span class="txt">Linkedin</span></a></li>
                                </ul>
                            </div>


                        </div>
                    </div>
                </div>

                <!--Sidebar Side-->
{{--                @include('layouts.sidebar')--}}
            </div>
        </div>
    </div>
@endsection
