@extends('layouts.master')
@section('content')
            <div class="background" data-aos="fade-in">
                @php
                    $check_format = !empty($page_cover) ? explode(".",$page_cover->content)[count(explode(".",$page_cover->content))-1] : null;
                @endphp
                @if(!empty($page_cover))
                    @if((strtolower($check_format) == 'mp4' || strtolower($check_format) == 'avi' || strtolower($check_format) == 'ogg' || strtolower($check_format) == 'webm'))
                        <video
                            class="background-video"
                            poster="{{!empty($page_cover) ? $page_cover->content : ''}}"
                            playsinline="playsinline"
                            autoplay="autoplay"
                            muted="muted"
                            loop="loop"
                            data-src="{{!empty($page_cover) ? $page_cover->content : ''}}"
                        ></video>
                    @else
                        <img
                            class="background-image"
                            src="{{!empty($page_cover) ? $page_cover->content : ''}}"
                            data-aos="fade-in"
                            data-aos-delay="200"
                        />
                    @endif
                @endif
                <div
                    class="background-title"
                    data-aos="fade-down"
                    data-aos-delay="300"
                >
                    {!! translate($static_contents("about","title_1")->content,session('languageID')) !!}
                </div>
            </div>
            <div class="about page">
                <div class="container-wrapper">
                    <div class="container-xxs">
                        <div class="row">
                            <div class="col-xl-10 offset-xl-1">
                                <div class="about-info">
                                    <h2
                                        class="about-info-title"
                                        data-aos="fade-up"
                                        data-aos-delay="100"
                                    >
                                        {!! translate($static_contents("about","title_2")->content,session('languageID')) !!}
                                    </h2>
                                    <div class="about-info-wrapper">
                                        <div class="about-info-left">
                                            <div
                                                class="about-info-left-text"
                                                data-aos="fade-in"
                                                data-aos-delay="500"
                                            >
                                                {!! translate($static_contents("about","title_2_text")->content,session('languageID')) !!}
                                            </div>
                                        </div>
                                        <div class="about-info-right">
                                            <div class="about-info-gallery lg">
                                                <div
                                                    class="about-info-gallery-item"
                                                    data-aos="zoom-in"
                                                    data-aos-delay="500"
                                                    data-src="{{URL::to('assets/images/gallery.jpeg')}}"
                                                >
                                                    <div class="gallery_item">
                                                        <a class="gallery_item_link lg-item"
                                                        ><img src="{{URL::to('assets/images/gallery.jpeg')}}" /><span
                                                                class="fa fa-search gallery_item-zoom"
                                                            ></span
                                                            ></a>
                                                    </div>
                                                </div>
                                                <div
                                                    class="about-info-gallery-item"
                                                    data-aos="zoom-in"
                                                    data-aos-delay="600"
                                                    data-src="{{URL::to('assets/images/gallery.jpeg')}}"
                                                >
                                                    <div class="gallery_item">
                                                        <a class="gallery_item_link lg-item"
                                                        ><img src="{{URL::to('assets/images/gallery.jpeg')}}" /><span
                                                                class="fa fa-search gallery_item-zoom"
                                                            ></span
                                                            ></a>
                                                    </div>
                                                </div>
                                                <div
                                                    class="about-info-gallery-item"
                                                    data-aos="zoom-in"
                                                    data-aos-delay="700"
                                                    data-src="{{URL::to('assets/images/gallery.jpeg')}}"
                                                >
                                                    <div class="gallery_item">
                                                        <a class="gallery_item_link lg-item"
                                                        ><img src="{{URL::to('assets/images/gallery.jpeg')}}" /><span
                                                                class="fa fa-search gallery_item-zoom"
                                                            ></span
                                                            ></a>
                                                    </div>
                                                </div>
                                                <div
                                                    class="about-info-gallery-item"
                                                    data-aos="zoom-in"
                                                    data-aos-delay="800"
                                                    data-src="{{URL::to('assets/images/gallery.jpeg')}}"
                                                >
                                                    <div class="gallery_item">
                                                        <a class="gallery_item_link lg-item"
                                                        ><img src="{{URL::to('assets/images/gallery.jpeg')}}" /><span
                                                                class="fa fa-search gallery_item-zoom"
                                                            ></span
                                                            ></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-10 offset-xl-1">
                                <div class="about-services">
                                    <div class="about-services-title">
                                        <div class="block_title" data-aos="fade-up">
                                            <div class="block_title-text">{!! translate($static_contents("about","our_services")->content,session('languageID')) !!}</div>
                                        </div>
                                    </div>
                                    <div class="about-services-list">
                                        @foreach($services as $service)
                                        <div class="about-services-list-item" data-aos="fade-up">
                                            <div class="service_item service_item-standard">
                                                <div class="service_item-image">
                                                    <img src="{{!empty($service->images[0]) ? $service->images[0]['full_files']['full_path'] : ''}}" />
                                                </div>
                                                <div class="service_item-right">
                                                    <div class="service_item-title">{{$service->title}}</div>
                                                    <div class="service_item-text">
                                                        {!!  $service->text !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-10 offset-xl-1">
                                <div class="about-gallery">
                                    <div class="block_title" data-aos="fade-up">
                                        <div class="block_title-text">{!! translate($static_contents("about","gallery")->content,session('languageID')) !!}</div>
                                    </div>
                                    <div class="about-gallery-swiper-wrap" data-aos="fade-down">
                                        <div class="about-gallery-swiper swiper-container">
                                            <div class="swiper-wrapper">
                                                @foreach($galleries as $gallery)
                                                <div class="swiper-slide about-gallery-swiper-slide">
                                                    <img src="{{$gallery->image}}" alt="{{$gallery->alt}}" />
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="about-gallery-swiper-buttons">
                                            <button class="about-gallery-swiper-prev button-left">
                                                <span class="fa fa-angle-left"></span></button
                                            ><button class="about-gallery-swiper-next button-right">
                                                <span class="fa fa-angle-right"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('layouts.contacts')
@endsection
