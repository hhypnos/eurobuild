@extends('layouts.master')
@section('content')
    <div class="product_inner page">
        <div class="container-wrapper">
            <div class="container-xxs">
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <div
                            class="product_inner-info-back-div"
                            data-aos="fade-down"
                            data-aos-delay="100"
                        >
                            <a class="product_inner-info-back" href="/"
                            ><span class="fa fa-angle-left"> </span>{!! translate($static_contents("products_inner","go_back")->content,session('languageID')) !!}</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <div class="product_inner-info">
                            <div class="product_inner-info-left">
                                <img
                                    class="product_inner-image"
                                    src="{{!empty($product->covers[0]) ? $product->covers[0]['full_files']['full_path'] : ''}}"
                                    data-aos="fade-up"
                                    data-aos-delay="300"
                                />
                            </div>
                            <div class="product_inner-info-right">
                                <div>
                                    <div
                                        class="product_inner-info-title"
                                        data-aos="fade-up"
                                        data-aos-delay="500"
                                    >
                                        {{$product->title}}
                                    </div>
                                    <div
                                        class="product_inner-info-price"
                                        data-aos="fade-up"
                                        data-aos-delay="600"
                                    >
                                        {!! translate($static_contents("products_inner","price")->content,session('languageID')) !!}: {{$product->price}}
                                    </div>
                                    <div
                                        class="product_inner-info-description"
                                        data-aos="fade-in"
                                        data-aos-delay="700"
                                    >
                                        <div
                                            class="product_inner-info-description-title"
                                            data-aos="fade-up"
                                            data-aos-delay="800"
                                        >
                                            {!! translate($static_contents("products_inner","product_description")->content,session('languageID')) !!}
                                        </div>
                                        <div
                                            class="product_inner-info-description-desc"
                                            data-aos="fade-up"
                                            data-aos-delay="300"
                                        >
                                            {!! $product->text  !!}

                                        </div>
                                    </div>
                                    <button
                                        class="product_inner-info-button"
                                        data-aos="fade-up"
                                        data-aos-delay="300"
                                    >
                                        {!! translate($static_contents("products_inner","order")->content,session('languageID')) !!}
                                    </button>
                                    <form class="product_inner-form" action="{{URL::to('send-order')}}" method="POST">
                                        {{csrf_field()}}
                                        <div
                                            class="form-input-wrap"
                                            data-aos="fade-up"
                                            data-aos-delay="1000"
                                        >
                                            <input
                                                class="form-input"
                                                data-required="true"
                                                data-name="phone"
                                                type="text"
                                                placeholder="{!! translate($static_contents("products_inner","placeholder_mob")->content,session('languageID')) !!}"
                                                name="phone"
                                            />
                                        </div>
                                        <div
                                            class="form-input-wrap form-input-checkbox"
                                            data-aos="fade-up"
                                            data-aos-delay="1000"
                                        >
                                            <label for="delivery_service">{!! translate($static_contents("products_inner","placeholder_delivery")->content,session('languageID')) !!}</label><input
                                                class="form-input"
                                                id="delivery_service"
                                                data-name="delivery"
                                                type="checkbox"
                                                name="is_delivery"
                                            />
                                        </div>
                                        <div
                                            class="form-input-wrap"
                                            data-aos="fade-up"
                                            data-aos-delay="1000"
                                        >
                                            <input
                                                class="form-input"
                                                data-required="true"
                                                data-name="address"
                                                type="text"
                                                placeholder="{!! translate($static_contents("products_inner","placeholder_address")->content,session('languageID')) !!}"
                                                name="address"
                                            />
                                        </div>
                                        <div
                                            class="form-input-wrap form-input-checkbox"
                                            data-aos="fade-up"
                                            data-aos-delay="1000"
                                        >
                                            <label for="load_service">{!! translate($static_contents("products_inner","placeholder_download")->content,session('languageID')) !!}</label><input
                                                class="form-input"
                                                id="load_service"
                                                data-name="load"
                                                type="checkbox"
                                                name="is_download"
                                            />
                                        </div>
                                        <div
                                            class="form-input-wrap form-input-wrap-last"
                                            data-aos="fade-up"
                                            data-aos-delay="1000"
                                        >
                            <textarea
                                class="form-input"
                                data-required="true"
                                data-name="message"
                                placeholder="{!! translate($static_contents("products_inner","placeholder_message")->content,session('languageID')) !!}"
                                rows="9"
                                name="message"
                            ></textarea>
                                        </div>
                                        <button
                                            class="form-button"
                                            type="submit"
                                            data-aos="fade-up"
                                            data-aos-delay="1000"
                                        >
                                            {!! translate($static_contents("products_inner","placeholder_send")->content,session('languageID')) !!}
                                        </button>
                                    </form>
                                </div>
                                <div
                                    class="product_inner-info-button_contact-wrap"
                                    data-aos="fade-up"
                                    data-aos-delay="1200"
                                >
                                    <a
                                        class="product_inner-info-button_contact"
                                        href="tel:{!! translate($static_contents("products_inner","contact_mob")->content,session('languageID')) !!}"
                                    >{!! translate($static_contents("products_inner","contact_us")->content,session('languageID')) !!}<span class="fa fa-phone"></span
                                        ></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(count($products)>0)
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <div class="product_inner-other">
                            <div class="block_title" data-aos="fade-up">
                                <div class="block_title-text">{!! translate($static_contents("products_inner","same_products")->content,session('languageID')) !!}</div>
                            </div>
                            <div class="product_inner-other-list">
                                @foreach($products as $prod)
                                <div class="product_inner-other-list-item">
                                    <div
                                        class="product_item"
                                        data-aos="fade-in"
                                        data-aos-delay="200"
                                    >
                                        <a
                                            class="product_item-link"
                                            href="{{URL::to('products/'.$prod->slug)}}"
                                        ><div>
                                                <div
                                                    class="product_item-image"
                                                    data-aos="fade-in"
                                                    data-aos-delay="300"
                                                >
                                                    <img src="{{!empty($prod->images[0]) ? $prod->images[0]['full_files']['full_path'] : ''}}" />
                                                </div>
                                                <div
                                                    class="product_item-title"
                                                    data-aos="fade-up"
                                                    data-aos-delay="500"
                                                >
                                                    {{$prod->title}}
                                                </div>
                                            </div>
                                            <div
                                                class="product_item-more"
                                                data-aos="fade-right"
                                                data-aos-delay="600"
                                            >
                                                {!! translate($static_contents("products_inner","read_more")->content,session('languageID')) !!}
                                            </div></a>
                                    </div>
                                </div>
                                    @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                    @endif
            </div>
        </div>
    </div>
    <div class="notification {{session()->has('success') ? session('success.open') : ''}}">
        <button class="notification-close">
            <span class="fa fa-times"></span>
        </button>
        <div class="notification-title">{{session('success.msg')}}</div>
        <div class="notification-description">
            {!! translate($static_contents("notify","address")->content,session('languageID')) !!}
        </div>
    </div>
@endsection
