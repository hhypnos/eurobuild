@extends('layouts.master')
@section('content')
        <div class="home page">
            <div class="slider">
                <div class="swiper-container slider-swiper-container">
                    <div class="swiper-wrapper">
                       @foreach($page_galleries as $page_gallery)
                            <div class="swiper-slide slider-swiper-slide">
                                <div class="background" data-aos="fade-in">
                                    <img
                                        class="background-image"
                                        src="{{$page_gallery->content}}"
                                        data-aos="fade-in"
                                        data-aos-delay="200"
                                    />
                                    <div
                                        class="background-title"
                                        data-aos="fade-down"
                                        data-aos-delay="300"
                                    ></div>
                                </div>
                            </div>
                           @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
            <div class="home-info">
                <div class="container-wrapper">
                    <div class="container-xxs">
                        <div class="row">
                            <div class="col-xl-10 offset-xl-1 col-xxs-12">
                                <div class="block_title" data-aos="fade-up">
                                    <div class="block_title-text">{!! translate($static_contents("home","title_1")->content,session('languageID')) !!}</div>
                                </div>
                                <div class="home-info-content">
                                   {!! translate($static_contents("home","title_text")->content,session('languageID')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(count($services)>0)
            <div class="home-services">
                <div class="container-wrapper">
                    <div class="container-xxs">
                        <div class="row">
                            <div class="col-xl-10 offset-xl-1 col-xxs-12">
                                <div class="block_title" data-aos="fade-up">
                                    <div class="block_title-text"> {!! translate($static_contents("home","title_2")->content,session('languageID')) !!}</div>
                                </div>
                                <div class="home-services-list">
                                    @foreach($services as $service)
                                    <div class="home-services-list-item" data-aos="fade-up">
                                        <div class="home-services-list-item-content">
                                            <img src="{{!empty($service->images[0]) ? $service->images[0]['full_files']['full_path'] : ''}}" />
                                            <div class="home-services-list-item-content-texts">
                                                <div>
                                                    <div
                                                        class="
                                  home-services-list-item-content-texts-title
                                "
                                                    >
                                                        {{$service->title}}
                                                    </div>
                                                    <div
                                                        class="
                                  home-services-list-item-content-texts-text
                                "
                                                    >
                                                       {!!  $service->description!!}
                                                    </div>
                                                </div>
                                                <a
                                                    class="home-services-list-item-content-texts-link"
                                                    href="{{URL::to('about')}}"
                                                >იხილეთ ვრცლად</a
                                                >
                                            </div>
                                        </div>
                                    </div>
                                        @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(count($news)>0)
            <div class="home-news">
                <div class="container-wrapper">
                    <div class="container-xxs">
                        <div class="row">
                            <div class="col-xl-10 offset-xl-1 col-lg-12 col-xxs-12">
                                <div class="block_title" data-aos="fade-up">
                                    <div class="block_title-text">{!! translate($static_contents("home","title_3")->content,session('languageID')) !!}</div>
                                </div>

                                    <div class="swiper-container home-news-swiper-container">
                                        <div class="swiper-wrapper">
                                            @foreach($news as $post)
                                            <div class="swiper-slide home-news-swiper-slide">
                                                <div class="news_item">
                                                    <a class="news_item-link" href="{{URL::to('news/'.$post->slug)}}"><div class="news_item-top">
                                                            <div
                                                                class="news_item-image"
                                                                data-aos="fade-down"
                                                                data-aos-delay="300"
                                                            >
                                                                <img src="{{!empty($post->images[0]) ? $post->images[0]['full_files']['full_path'] : ''}}" />
                                                            </div>
                                                            <div
                                                                class="news_item-title"
                                                                data-aos="fade-up"
                                                                data-aos-delay="400"
                                                            >
                                                                {{$post->title}}
                                                            </div>
                                                            <div
                                                                class="news_item-description"
                                                                data-aos="fade-in"
                                                                data-aos-delay="400"
                                                            >
                                                               {!! $post->description !!}
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="news_item-footer"
                                                            data-aos="slide-right"
                                                            data-aos-delay="500"
                                                        >
                                                            <div class="news_item-date">{{\Carbon\Carbon::parse($post->created_at)->format('d M Y')}}</div>
                                                            <div class="news_item-more">სრულად</div>
                                                        </div></a>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>

                                <div class="home-news-swiper-buttons">
                                    <button class="home-news-swiper-prev button-left">
                                        <span class="fa fa-angle-left"></span></button
                                    ><button class="home-news-swiper-next button-right">
                                        <span class="fa fa-angle-right"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @include("layouts.contacts")
            @if(count($vacancies) > 0)
            <div class="vacancies">
                <div class="container-wrapper">
                    <div class="container-xxs">
                        <div class="row">
                            <div class="col-xl-8 offset-xl-2 col-lg-12 col-xxs-12">
                                <div class="vacancies-header">
                                    <div class="block_title" data-aos="fade-up">
                                        <div class="block_title-text">{!! translate($static_contents("home","title_4")->content,session('languageID')) !!}</div>
                                    </div>
                                    <a
                                        class="vacancies-more"
                                        href="{{URL::to('vacancy')}}"
                                        data-aos="fade-up"
                                        data-aos-delay="300"
                                    >იხილეთ სრულად</a
                                    >
                                </div>
                                <div class="vacancies-list">
                                    @foreach($vacancies as $vacancy)
                                    <div class="vacancies-list-item" data-aos="fade-up">
                                        <div class="vacancy">
                                            <div class="vacancy-top">
                                                <div class="vacancy-title">
                                                    {{$vacancy->title}}
                                                </div>
                                                <div class="vacancy-right">
                                                    <div class="vacancy-top-item">
                                                        <div class="vacancy-top-item-content">
                                                            <div class="vacnacy-top-item-title">
                                                                გამოქვეყნდა
                                                            </div>
                                                            <div class="vacancy-top-item-value">
                                                                {{date('Y-m-d',strtotime($vacancy->created_at))}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="vacancy-top-item">
                                                        <div class="vacancy-top-item-content">
                                                            <div class="vacnacy-top-item-title">
                                                                ბოლო ვადა
                                                            </div>
                                                            <div class="vacancy-top-item-value">
                                                                {{$vacancy->expiration_date}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="vacancy-button">
                                                        <button class="vacancy-button-btn">
                                                            დეტალურად<span
                                                                class="fa fa-angle-down"
                                                            ></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vacancy-bot">
                                                {!!  $vacancy->description !!}
                                            </div>
                                        </div>
                                    </div>
                                        @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                @endif
        </div>
        @if(count($notes)>0)
        <div class="feature" data-aos="fade-up">
            <div class="container-wrapper">
                <div class="container-xxs">
                    <div class="row">
                        <div class="col-xl-10 offset-xl-1 col-xxs-12">
                            <div class="swiper-container feature-swiper-container">
                                <div class="swiper-wrapper">
                                  @foreach($notes as $note)
                                        <div class="swiper-slide">
                                            <div class="feature-swiper-slide-item">
                                                <img src="{{!empty($note->images[0]) ? $note->images[0]['full_files']['full_path'] : ''}}" />
                                                <div class="feature-swiper-slide-item-content">
                                                    <div
                                                        class="feature-swiper-slide-item-content-title"
                                                    >
                                                        {{$note->title}}
                                                    </div>
                                                    <div
                                                        class="
                                feature-swiper-slide-item-content-info-price
                              "
                                                    >
                                                        {!! $note->subtitle !!}

                                                    </div>
                                                    <div
                                                        class="
                                feature-swiper-slide-item-content-info-text
                              "
                                                    >
                                                        {!! $note->description !!}

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      @endforeach
                                </div>
                            </div>
                            <div class="feature-swiper-navigation">
                                <button class="feature-swiper-navigation-prev">
                                    <span class="fa fa-angle-left"></span></button
                                ><button class="feature-swiper-navigation-next">
                                    <span class="fa fa-angle-right"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="notification">
            <button class="notification-close">
                <span class="fa fa-times"></span>
            </button>
            <div class="notification-title">ნოთიფიკაციის სახელი</div>
            <div class="notification-description">
                მისამართი: საქართველო, თბილისი 0131, აღმაშენებლის ხეივანი 12 კმ,
                ფარსადანის 7 ცხელი ხაზი: 0322 52 43 43
            </div>
        </div>



@endsection
