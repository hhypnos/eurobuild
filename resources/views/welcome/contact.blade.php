@extends('layouts.master')
@section('content')
            <div class="contact-page page">
                <div class="contact-page-content">
                    <div class="container-wrapper">
                        <div class="container-xxs">
                            <div class="row">
                                <div class="col-xxs-12">
                                    <div
                                        class="contact-page-content-card"
                                        data-aos="fade-in"
                                        data-aos-delay="500"
                                    >
                                        <div class="contact-page-left">
                                            <div
                                                class="contact-page-title contact-page-left-title"
                                                data-aos="fade-up"
                                                data-aos-delay="800"
                                            >
                                                {!! translate($static_contents("contact","title_1")->content,session('languageID')) !!}
                                            </div>
                                            <form class="contact-page-form" action="{{URL::to('send-mail')}}" method="post">

                                                <div
                                                    class="form-input-wrap"
                                                    data-aos="fade-up"
                                                    data-aos-delay="1000"
                                                >
                                                    {{csrf_field()}}
                                                    <input
                                                        class="form-input"
                                                        data-required="true"
                                                        data-name="name"
                                                        type="text"
                                                        placeholder="სახელი *"
                                                        name="full_name"
                                                    />
                                                </div>
                                                <div
                                                    class="form-input-wrap"
                                                    data-aos="fade-up"
                                                    data-aos-delay="1000"
                                                >
                                                    <input
                                                        class="form-input"
                                                        data-required="true"
                                                        data-name="phone"
                                                        type="text"
                                                        placeholder="მობ ნომერი *"
                                                        name="phone"
                                                    />
                                                </div>
                                                <div
                                                    class="form-input-wrap"
                                                    data-aos="fade-up"
                                                    data-aos-delay="1000"
                                                >
                                                    <input
                                                        class="form-input"
                                                        data-name="email"
                                                        type="text"
                                                        placeholder="ელფოსტა *"
                                                        name="email"
                                                    />
                                                </div>
                                                <div
                                                    class="form-input-wrap"
                                                    data-aos="fade-up"
                                                    data-aos-delay="1000"
                                                >
                            <textarea
                                class="form-input"
                                data-required="true"
                                data-name="message"
                                placeholder="შეტყობინება *"
                                rows="9"
                                name="message"
                            ></textarea>
                                                </div>
                                                <button
                                                    class="form-button"
                                                    type="submit"
                                                    data-aos="fade-up"
                                                    data-aos-delay="1000"
                                                >
                                                    {!! translate($static_contents("contact","send")->content,session('languageID')) !!}
                                                </button>
                                            </form>
                                        </div>
                                        <div class="contact-page-right">
                                            <div
                                                class="contact-page-title"
                                                data-aos="fade-up"
                                                data-aos-delay="1000"
                                            >
                                                {!! translate($static_contents("contact","title_2")->content,session('languageID')) !!}
                                            </div>
                                            <div class="contact-info">
                                                <div
                                                    class="contact-address"
                                                    data-aos="fade-up"
                                                    data-aos-delay="1000"
                                                >
                                                    {!! translate($static_contents("contact","title_2_text")->content,session('languageID')) !!}

                                                </div>
                                                <div
                                                    class="contact-hotline"
                                                    data-aos="fade-up"
                                                    data-aos-delay="1000"
                                                >
                                                    <a href="tel:557602948"
                                                    > {!! translate($static_contents("contact","title_2_text2")->content,session('languageID')) !!}</a
                                                    >
                                                </div>
                                                <div
                                                    class="contact-mail"
                                                    data-aos="fade-up"
                                                    data-aos-delay="1000"
                                                >
                                                    <a href="mailto:contact@weve.tech"></a>
                                                </div>
                                                <div
                                                    class="contact-map"
                                                    data-aos="fade-up"
                                                    data-aos-delay="1000"
                                                >
                                                    <a
                                                        href="https://www.google.com/maps/place/%E1%83%95%E1%83%90%E1%83%96%E1%83%98%E1%83%A1%E1%83%A3%E1%83%91%E1%83%9C%E1%83%98%E1%83%A1+I+%E1%83%9B%2F%E1%83%A0+%237/@41.6940032,44.8495616,14z/data=!4m5!3m4!1s0x40440da167a9d50f:0xdc293f8f5c4423e0!8m2!3d41.7000715!4d44.8497421"
                                                        target="_BLANK"
                                                        data-aos="fade-up"
                                                        data-aos-delay="1000"
                                                    >{!! translate($static_contents("contact","find_on_map")->content,session('languageID')) !!}</a
                                                    >
                                                </div>
                                                <div
                                                    class="contact-networks"
                                                    data-aos="fade-up"
                                                    data-aos-delay="1000"
                                                >
                                                    <div
                                                        class="contact-networks-title"
                                                        data-aos="fade-up"
                                                        data-aos-delay="1000"
                                                    >
                                                        {!! translate($static_contents("contact","title_2_text3")->content,session('languageID')) !!}
                                                    </div>
                                                    <div class="network">
                                                        <div class="network-item">
                                                            <a href="{{translate($static_contents("contact","fb")->content,session('languageID'))}}"
                                                            ><span class="fa fa-facebook"> </span
                                                                ></a>
                                                        </div>
                                                        <div class="network-item">
                                                            <a href="{{translate($static_contents("contact","google")->content,session('languageID'))}}"
                                                            ><span class="fa fa-google"> </span
                                                                ></a>
                                                        </div>
                                                        <div class="network-item">
                                                            <a href="{{translate($static_contents("contact","instagram")->content,session('languageID'))}}"
                                                            ><span class="fa fa-instagram"></span
                                                                ></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contact-page-map" data-aos="fade-up" data-aos-delay="500">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23833.82780442233!2d44.849561599999994!3d41.6940032!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40440da7db8a95bf%3A0xff31d096d8472b09!2z4YOn4YOV4YOQ4YOg4YOa4YOY4YOhIOGDpeGDo-GDqeGDkA!5e0!3m2!1ska!2sge!4v1622941773584!5m2!1ska!2sge"
                        width="100%"
                        height="100%"
                        style="border: 0"
                        loading="lazy"
                    ></iframe>
                </div>
            </div>
            <div class="notification {{session()->has('success') ? session('success.open') : ''}}">
                <button class="notification-close">
                    <span class="fa fa-times"></span>
                </button>
                <div class="notification-title">{{session('success.msg')}}</div>
                <div class="notification-description">
                    {!! translate($static_contents("notify","address")->content,session('languageID')) !!}
                </div>
            </div>


@endsection
