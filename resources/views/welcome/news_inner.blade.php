@extends('layouts.master')
@section('content')
    <div class="background" data-aos="fade-in">
        <img
            class="background-image"
            src="{{!empty($news->covers[0]) ? $news->covers[0]['full_files']['full_path'] : ''}}"
            data-aos="fade-in"
            data-aos-delay="200"
        />
        <div
            class="background-title"
            data-aos="fade-down"
            data-aos-delay="300"
        >
            {{$news->title}}
        </div>
    </div>
    <div class="news_inner page">
        <div class="container-wrapper">
            <div class="container-xxs">
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <div class="news_inner-info">
                            <h2
                                class="news_inner-info-title"
                                data-aos="fade-up"
                                data-aos-delay="100"
                            >
                               {{$news->subtitle}}
                            </h2>
                            <div class="news_inner-info-wrapper">
                                <div class="news_inner-info-left">
                                    <div
                                        class="news_inner-info-left-text"
                                        data-aos="fade-in"
                                        data-aos-delay="500"
                                    >
                                        {!! $news->text !!}
                                    </div>
                                </div>
                                <div class="news_inner-info-right">
                                    <div class="news_inner-info-gallery lg">
                                        <div
                                            class="news_inner-info-gallery-item"
                                            data-aos="zoom-in"
                                            data-aos-delay="null"
                                            data-src="{{!empty($news->images[0]) ? $news->images[0]['full_files']['full_path'] : ''}}"
                                        >
                                            <div class="gallery_item">
                                                <a class="gallery_item_link lg-item"
                                                ><img src="{{!empty($news->images[0]) ? $news->images[0]['full_files']['full_path'] : ''}}" /><span
                                                        class="fa fa-search gallery_item-zoom"
                                                    ></span
                                                    ></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="news_inner-info-footer">
                            <div class="news_inner-info-date">{{\Carbon\Carbon::parse($news->created_at)->format('d M Y')}}</div>
                            <div class="news_inner-info-share">
                                <div
                                    class="fb-like"
                                    data-href="https://developers.facebook.com/docs/plugins/"
                                    data-width=""
                                    data-layout="standard"
                                    data-action="like"
                                    data-size="small"
                                    data-share="true"
                                ></div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(count($newses) > 0)
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <div class="news_inner-other">
                            <div class="block_title" data-aos="fade-up">
                                <div class="block_title-text">{!! translate($static_contents("news_inner","other_news")->content,session('languageID')) !!}</div>
                            </div>
                            <div class="news_inner-other-list">
                                @foreach($newses as $post)
                                    <div class="news_inner-other-list-item">
                                        <div class="news_item">
                                            <a class="news_item-link" href="{{URL::to('news/'.$post->slug)}}"
                                            ><div class="news_item-top">
                                                    <div
                                                        class="news_item-image"
                                                        data-aos="fade-down"
                                                        data-aos-delay="300"
                                                    >
                                                        <img src="{{!empty($post->images[0]) ? $post->images[0]['full_files']['full_path'] : ''}}" />
                                                    </div>
                                                    <div
                                                        class="news_item-title"
                                                        data-aos="fade-up"
                                                        data-aos-delay="400"
                                                    >
                                                        {{$post->title}}
                                                    </div>
                                                    <div
                                                        class="news_item-description"
                                                        data-aos="fade-in"
                                                        data-aos-delay="400"
                                                    >
                                                        {!! $post->description !!}
                                                    </div>
                                                </div>
                                                <div
                                                    class="news_item-footer"
                                                    data-aos="slide-right"
                                                    data-aos-delay="500"
                                                >
                                                    <div class="news_item-date">{{$post->created_at}}</div>
                                                    <div class="news_item-more">{!! translate($static_contents("news_inner","full")->content,session('languageID')) !!}</div>
                                                </div></a>
                                        </div>
                                    </div>
                                    @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                    @endif
            </div>
        </div>
    </div>
@include('layouts.contacts')
@endsection
