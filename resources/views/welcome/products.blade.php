@extends('layouts.master')
@section('content')
            <div class="background" data-aos="fade-in">
                @php
                    $check_format = !empty($page_cover) ? explode(".",$page_cover->content)[count(explode(".",$page_cover->content))-1] : null;
                @endphp
                @if(!empty($page_cover))
                    @if((strtolower($check_format) == 'mp4' || strtolower($check_format) == 'avi' || strtolower($check_format) == 'ogg' || strtolower($check_format) == 'webm'))
                        <video
                            class="background-video"
                            poster="{{!empty($page_cover) ? $page_cover->content : ''}}"
                            playsinline="playsinline"
                            autoplay="autoplay"
                            muted="muted"
                            loop="loop"
                            data-src="{{!empty($page_cover) ? $page_cover->content : ''}}"
                        ></video>
                    @else
                        <img
                            class="background-image"
                            src="{{!empty($page_cover) ? $page_cover->content : ''}}"
                            data-aos="fade-in"
                            data-aos-delay="200"
                        />
                    @endif
                @endif
                <div
                    class="background-title"
                    data-aos="fade-down"
                    data-aos-delay="300"
                >
                    {!! request('cat') ?  \App\Models\Navigation::where('slug','/products?cat='.request('cat'))->where('lang_id',session('languageID') ? session('languageID') : 1)->first()->title : translate($static_contents("products","title")->content,session('languageID')) !!}
                </div>
            </div>
            <div class="product page">
                <div class="container-wrapper">
                    <div class="container-xxs">
                        <div class="row">
                            <div class="col-xl-10 offset-xl-1 col-xxs-12">
                                @if(count($products) == 0)

                                    <div class="warn-text aos-animate" data-aos="fade-up" data-aos-delay="800" style="
    color: black;
    text-align: center;
    font-size: 30px;
    font-weight: bold;
    letter-spacing: 4px;
    width: 100%;
">პროდუქტების ეს კატეგორია ცარიელია</div>
                                @endif
                                <div class="product-list">
                                    @foreach($products as $product)
                                    <div
                                        class="product_item-wrap"
                                        data-aos="fade-up"
                                        data-aos-delay="200"
                                    >
                                        <div
                                            class="product_item"
                                            data-aos="fade-in"
                                            data-aos-delay="200"
                                        >
                                            <a class="product_item-link" href="{{URL::to('products/'.$product->slug)}}"
                                            ><div>
                                                    <div
                                                        class="product_item-image"
                                                        data-aos="fade-in"
                                                        data-aos-delay="300"
                                                    >
                                                        <img src="{{!empty($product->images[0]) ? $product->images[0]['full_files']['full_path'] : ''}}" />
                                                    </div>
                                                    <div
                                                        class="product_item-title"
                                                        data-aos="fade-up"
                                                        data-aos-delay="500"
                                                    >
                                                        {{$product->title}}
                                                    </div>
                                                </div>
                                                <div
                                                    class="product_item-more"
                                                    data-aos="fade-right"
                                                    data-aos-delay="600"
                                                >
                                                    {!! translate($static_contents("products","full")->content,session('languageID')) !!}
                                                </div></a>
                                        </div>
                                    </div>
                                        @endforeach

                                </div>
                            </div>
                        </div>
                        @include('layouts.paginate', ['paginator' => $products])
                    </div>
                </div>
            </div>
            @include("layouts.contacts")

@endsection
