@extends('layouts.master')
@section('content')
    <!--Breadcrumbs-->
{{--    @include("layouts.breadcrumbs")--}}
    <br><br>
    <section class="">
        <div class="lower-row">
            <div class="auto-container">
                <div class="row clearfix">
                    {{--                    col-xl-5 col-lg-6 col-md-12 col-sm-12--}}
                    <div class="text-col " style="text-align: center;width: 100%;">
                        <div class="inner">
                            <div class="sec-title with-separator">
                                <h2 id="static-text-first_title">{!! translate($static_contents("about","third_title")->content,session('languageID')) !!}</h2>
                                <div class="separator" style="left: 34%"><span class="cir c-1"></span><span class="cir c-2"></span><span class="cir c-3"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="highlights-section-two">
        <div class="image-layer" style="background-image:url({{URL::to('images/hub-georgia.jpg')}});"></div>
        <div class="auto-container">
            <div class="featured-carousel owl-theme owl-carousel" id="load-partner-slider">
                @foreach($partners as $partner)
                    <div class="featured-block-five">
                        <div class="inner-box">
                            {{--                        <div class="count-box"><span>{{$partner->order_id}}</span></div>--}}
                            <div class="content">
                                <div class="icon-box"><img src="{{$partner->logo}}" alt="{{$partner->title}}"/></span></div>
                                <h3><a href="{{$partner->url}}" target="_blank">{{$partner->title}}</a></h3>
                                <div class="text">{{$partner->description}}</div>
                                <div class="read-more"><a href="{{$partner->url}}" target="_blank"><span class="flaticon-right-2"></span></a></div>
                            </div>
                            <div class="bottom-text">{{$partner->title}}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
