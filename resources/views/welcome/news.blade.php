@extends('layouts.master')
@section('content')
        <div class="background" data-aos="fade-in">
            @php
                $check_format = !empty($page_cover) ? explode(".",$page_cover->content)[count(explode(".",$page_cover->content))-1] : null;
            @endphp
            @if(!empty($page_cover))
            @if((strtolower($check_format) == 'mp4' || strtolower($check_format) == 'avi' || strtolower($check_format) == 'ogg' || strtolower($check_format) == 'webm'))
                <video
                    class="background-video"
                    poster="{{!empty($page_cover) ? $page_cover->content : ''}}"
                    playsinline="playsinline"
                    autoplay="autoplay"
                    muted="muted"
                    loop="loop"
                    data-src="{{!empty($page_cover) ? $page_cover->content : ''}}"
                ></video>
                @else
                <img
                    class="background-image"
                    src="{{!empty($page_cover) ? $page_cover->content : ''}}"
                    data-aos="fade-in"
                    data-aos-delay="200"
                />
            @endif
            @endif
            <div
                class="background-title"
                data-aos="fade-down"
                data-aos-delay="300"
            >
                {!! translate($static_contents("news","title")->content,session('languageID')) !!}
            </div>
        </div>
        <div class="news page">
            <div class="container-wrapper">
                <div class="container-xxs">
                    <div class="row">

                        @foreach($news as $key => $post)

                        <div
                            class="
                    col-xl-4 col-lg-5 col-xxs-12 col-md-6
                    {{$key % 2 == 0 ? 'offset-xl-2 offset-lg-1':'' }}
                  "
                        >

                                <div class="news_item-wrap" data-aos="fade-up">
                                    <div class="news_item">
                                        <a class="news_item-link" href="{{URL::to('news/'.$post->slug)}}"
                                        >
                                            <div class="news_item-top">
                                                <div
                                                    class="news_item-image"
                                                    data-aos="fade-down"
                                                    data-aos-delay="300"
                                                >
                                                    <img src="{{!empty($post->images[0]) ? $post->images[0]['full_files']['full_path'] : ''}}"/>
                                                </div>
                                                <div
                                                    class="news_item-title"
                                                    data-aos="fade-up"
                                                    data-aos-delay="400"
                                                >
                                                    {{$post->title}}
                                                </div>
                                                <div
                                                    class="news_item-description"
                                                    data-aos="fade-in"
                                                    data-aos-delay="400"
                                                >
                                                    {!! $post->description !!}
                                                </div>
                                            </div>
                                            <div
                                                class="news_item-footer"
                                                data-aos="slide-right"
                                                data-aos-delay="500"
                                            >
                                                <div class="news_item-date">{{\Carbon\Carbon::parse($post->created_at)->format('d M Y')}}</div>
                                                <div class="news_item-more" >{!! translate($static_contents("news","read_more")->content,session('languageID')) !!}</div>
                                            </div>
                                        </a
                                        >
                                    </div>
                                </div>

                        </div>
                        @endforeach
                    </div>
                    @include('layouts.paginate', ['paginator' => $news])
                </div>
            </div>
        </div>
@include('layouts.contacts')
@endsection
