{{--IF NOT AJAX REQUEST THEN USE THIS ERRORS--}}
@if (count($errors))
    @foreach ($errors->all() as $error)
        <script type="text/javascript">
                $(function(){
                    new PNotify({
                    title: 'Warning',
                    text: '{{$error}}',
                    type:'{{ session('errorType')}}',
                    styling:'bootstrap3'
                });
                    });
            </script>
    @endforeach
@endif
