<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{$title}}</title>

    <!-- Bootstrap -->
    <link href="{{URL::to('assets/dashboard_assets/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{URL::to('assets/dashboard_assets/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{URL::to('assets/dashboard_assets/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="{{URL::to('assets/dashboard_assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet"/>
    <!-- iCheck style and skin -->
    <link rel="stylesheet" href="{{URL::to('assets/dashboard_assets/vendors/iCheck/skins/flat/green.css')}}">
    <link rel="stylesheet" href="{{URL::to('assets/dashboard_assets/vendors/switchery/dist/switchery.min.css')}}">
    <!-- Switchery -->
    <!-- Select2 -->
    <link href="{{URL::to('assets/dashboard_assets/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{URL::to('assets/dashboard_assets/build/css/custom.min.css')}}" rel="stylesheet">
    <!-- PNotify -->
    <link href="{{URL::to('assets/dashboard_assets/vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{URL::to('assets/dashboard_assets/vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{URL::to('assets/dashboard_assets/vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
    <!-- jQuery -->
    <script src="{{URL::to('assets/dashboard_assets/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Tiny library -->
    @include('dashboard_layouts.tiny')

</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="#" class="site_title"><i class="fa fa-paw"></i> <span>{{$title}}</span></a>
                </div>

                <div class="clearfix"></div>

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{URL::to('apanel')}}">Gallery</a></li>
                                    <li><a href="{{URL::to('apanel/section-work')}}">News</a></li>
                                    <li><a href="{{URL::to('apanel/section-products')}}">Products</a></li>
                                    <li><a href="{{URL::to('apanel/section-notes')}}">Notes</a></li>
                                    <li><a href="{{URL::to('apanel/section-vacancies')}}">Vacancies</a></li>
                                    <li><a href="{{URL::to('apanel/section-services')}}">Services</a></li>
                                    <li><a href="{{URL::to('apanel/section-orders')}}">Orders</a></li>
                                    <li><a href="{{URL::to('apanel/section-content')}}">Static Content</a></li>
                                    <li><a href="{{URL::to('apanel/home-page-gallery')}}">Home Page Gallery</a></li>
                                    <li><a href="{{URL::to('apanel/section-categories')}}">Categories</a></li>
{{--                                    <li><a href="{{URL::to('apanel/section-news')}}">News</a></li>--}}
                                </ul>
                            </li>
                            <li><a><i class="fa fa-edit"></i> Contents <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{URL::to('apanel/add-news')}}">Add Newses</a></li>
                                    <li><a href="{{URL::to('apanel/add-products')}}">Add Products</a></li>
                                    <li><a href="{{URL::to('apanel/add-notes')}}">Add Notes</a></li>
                                    <li><a href="{{URL::to('apanel/add-services')}}">Add Services</a></li>
                                    <li><a href="{{URL::to('apanel/add-vacancies')}}">Add Vacancies</a></li>
                                    <li><a href="{{URL::to('apanel/add-logo')}}">Add Logo</a></li>
{{--                                    <li><a href="{{URL::to('apanel/add-work')}}">Add Works</a></li>--}}
                                </ul>
                            </li>

                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{URL::to('logout')}}">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                               {{auth()->user()->first_name}} {{auth()->user()->last_name}}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="{{URL::to('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
