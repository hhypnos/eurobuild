@extends('dashboard_layouts.master')
@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="wholecontent">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{$title}}</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{$title}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content2">

                            <div class="row">
                                @foreach($cover_images as $cover_image)
                                <div class="col-md-55">
                                    <div class="thumbnail">
                                        <div class="image view view-first" style="height: 100%;">
                                            <img style="width: 100%; display: block;" src="{{URL::to('/'.$cover_image->image)}}" alt="image" />
                                            <div class="mask">
                                                <div class="tools tools-bottom">
                                                    <a href="{{URL::to('/'.$cover_image->image)}}" target="_blank"><i class="fa fa-link"></i></a>
                                                    <a href="#"><i data-imageid="{{$cover_image->image_id}}" onclick="return confirm('Are you sure you want to delete image ?')? deleteimagefromcoverimages(this) : '' " class="fa fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <form id="upload" method="post" action="{{URL::to('apanel/coverimages/uploadcoverimages')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div id="drop">
                    Drop Here

                    <a>Browse</a>

                    <input type="file" name="upl[]" multiple />
                </div>

                <ul>
                    <!-- The file uploads will be shown here -->
                </ul>

            </form>
        </div>
    </div>

    <script>
        $("form").submit((e) => {
            e.preventDefault();
        });
            function deleteimagefromcoverimages(element) {
                var CSRF_TOKEN = "{{csrf_token()}}";
                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{URL::to('apanel/coverimages/deletecoverimages')}}',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, image_id: $(element).data('imageid')},
                    /* remind that 'data' is the response of the AjaxController */

                    success: function (data) {
                        //refresh section
                        new PNotify({
                            title: 'Image Deleted ',
                            text: '',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                        refreshContent()
                    },

                    error: function (request, status, error) {
                        for (x in request.responseJSON['errors']) {
                            new PNotify({
                                title: 'Error',
                                text: request.responseJSON['errors'][x],
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }
                    }

                });
            }

        function refreshContent() {
            $( ".x_content2").load(window.location.href + " .x_content2");
        }
    </script>
    <!-- /page content -->
@endsection