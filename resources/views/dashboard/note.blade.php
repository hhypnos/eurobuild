@extends('dashboard_layouts.master')
@section('content')
    <style>
        .grid-object-container {
            width:100%;
            display: flex;
            flex-wrap: wrap;
        }
        .grid-object-container .grid-object-x2{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:50%;
            float:left;
            padding:20px;
        }
        .grid-object-container .grid-object-x3{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:33.33%;
            float:left;
            padding:20px;
        }

        .grid-object-container .grid-object-x4{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:25%;
            float:left;
            padding:20px;
        }
        .grid-object-container .grid-object-x5{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:20%;
            float:left;
            padding:20px;
        }
        .grid-object-container .grid-object-x6{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:16.66%;
            float:left;
            padding:20px;
        }

        @media(max-width:800px){
            .grid-object-container .grid-object-x2{
                width:50%;
            }
            .grid-object-container .grid-object-x3{
                width:50%;
            }
            .grid-object-container .grid-object-x4{
                width:50%;
            }
            .grid-object-container .grid-object-x5{
                width:50%;
            }
            .grid-object-container .grid-object-x6{
                width:50%;
            }
        }
        @media(max-width:500px){
            .grid-object-container .grid-object-x2{
                width:100%;
            }
            .grid-object-container .grid-object-x3{
                width:100%;
            }
            .grid-object-container .grid-object-x4{
                width:100%;
            }
            .grid-object-container .grid-object-x5{
                width:100%;
            }
            .grid-object-container .grid-object-x6{
                width:100%;
            }
        }
    </style>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="wholecontent">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{$post->title}}</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{$post->subtitle}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <style>
                                .checkbox{
                                    display: inline-block;
                                }
                            </style>
                            <form class="form-horizontal form-label-left" id="updatenote" method="post" action="{{URL::to('apanel/post/updatenote')}}">
                                {{csrf_field()}}
                                @php($get_section = null)
                                @php($draft=true)
                                @php($link = "drafts")
                                 <br><br>
                                <span class="section">Data</span>
                                        <input type="hidden" name="post_id" value="{{$post->post_id}}">
                                <input type="hidden" name="postID" value="{{$post->post_id}}">
                                <input type="hidden" name="type_id" value="{{$post->type_id}}">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Language">Language <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="language" class="form-control col-md-7 col-xs-12" name="default_lang" required="required" >
                                            <option value="2">English</option>
                                            <option value="1">ქართული</option>
                                            <option value="3">Руский</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="title" class="form-control col-md-7 col-xs-12" name="title" placeholder="" required="required" value="{{$post->title}}" type="text">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Subtitle <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="subtitle" required="required"  value="{{$post->subtitle}}" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <br><br><br>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Description <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="content-editor" class="content-editor" name="postText" required="required" class="form-control col-md-7 col-xs-12">{!! $post->description !!}</textarea>
                                    </div>
                                </div>
                                <br><br><br>


                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button  id="cancel" onclick="event.preventDefault();(function(){window.history.back();})();" class="btn btn-primary">Cancel</button>
{{--                                        <a href="{{route('translated_post',array($post->post_id,1))}}" class="btn btn-warning">Languages</a>--}}

                                        <button id="send" type="submit"class="btn btn-success">Save</button>
                                        <button id="delete" onclick="event.preventDefault();(function(){$('#my-confirm-dialog').show(); })();" class="btn btn-danger">Delete</button>
                                    </div>

                                </div>
                            </form>
                            <br><br><br>
                            <hr>
                        </div>
                        <div class="x_content2">

                            <div class="row">
                                @foreach($post->images as $image)
                                <div class="col-md-55">
                                    <div class="thumbnail">
                                        <div class="image view view-first" style="height: 100%;">
                                            <img style="width: 100%; display: block;" src="{{$image['full_files']['full_path']}}" alt="image" />
                                            <div class="mask">
                                                <div class="tools tools-bottom">
                                                <a href="{{$image['full_files']['full_path']}}" target="_blank"><i class="fa fa-link"></i></a>

                                                    <a href="#"><i data-imageid="{{$image['full_files']->id}}" data-contentid="{{$post->post_id}}" data-deleteurl="{{URL::to('apanel/post/deleteimage')}}" data-relationtable="App\Models\Note" onclick="deleteimagefrompost(this)" class="fa fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach

                            </div>
                        </div>

                        <div class="x_content2">

                            <div class="row">
{{--                                @foreach($post->covers as $image)--}}
{{--                                    <div class="col-md-55">--}}
{{--                                        <div class="thumbnail">--}}
{{--                                            <div class="image view view-first" style="height: 100%;">--}}
{{--                                                <img style="width: 100%; display: block;" src="{{$image['full_files']['full_path']}}" alt="image" />--}}
{{--                                                <div class="mask">--}}
{{--                                                    <div class="tools tools-bottom">--}}
{{--                                                        <a href="{{$image['full_files']['full_path']}}" target="_blank"><i class="fa fa-link"></i></a>--}}

{{--                                                        <a href="#"><i data-imageid="{{$image['full_files']->id}}" data-contentid="{{$post->post_id}}" data-deleteurl="{{URL::to('apanel/post/deleteimage')}}" data-relationtable="App\Models\Note" onclick="deleteimagefrompost(this)" class="fa fa-times"></i></a>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endforeach--}}

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <form id="upload" method="post" action="{{URL::to('apanel/post/uploadimage')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="post_id" value="{{$post->post_id}}">
                <input type="hidden" name="image_genderID" value="1">
                <input type="hidden" name="relation_table" value="App\Models\Note">
                <input type="hidden" name="type_id" value="1">
                <div id="drop">
                    Main Image

                    <a>Browse</a>

                    <input type="file" name="upl[]" multiple />
                </div>

                <ul>
                    <!-- The file uploads will be shown here -->
                </ul>
            </form>
            <br><br><br>

            <br><br><br><br><br>
        </div>
    </div>

    <div id="my-confirm-dialog" class="dialog-overlay">

        <div class="dialog-card">

            <div class="dialog-question-sign"><i class="fa fa-question"></i></div>

            <div class="dialog-info">

                <h5>Are you sure?</h5>
                <p>Post Can't be restored</p>

                <button class="dialog-confirm-button" onclick="deletePost()">Yes</button>
                <button class="dialog-reject-button" onclick='(function(){$("#my-confirm-dialog").hide(); })();'>No</button>

            </div>

        </div>

    </div>

    <script>
        $("form").submit((e) => {
            e.preventDefault();
        });

        let deletepublishedposttextimage = (element)=>{
            event.preventDefault();
            if(!confirm('Are you sure you want to delete this image?')){
                return;
            }
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/post/deleteposttextimage')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token:'{{csrf_token()}}', imageID:$(element).data("textimageid")},
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //remove input
                    $(element).closest(".deletepublishedposttextimage").remove();
                    new PNotify({
                        title: 'Post ',
                        text: 'Post Text Image Removed',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    for (x in request.responseJSON['errors']) {
                        new PNotify({
                            title: 'Error',
                            text: request.responseJSON['errors'][x],
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }

            });
        };

        let deletepublishedposttext = (element)=>{
            event.preventDefault();
            if(!confirm('Are you sure you want to delete this text?')){
                return;
            }
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/post/deleteposttext')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: $(element).closest("form").serialize(),
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //remove input
                    $(element).closest("form").remove();
                    new PNotify({
                        title: 'Post ',
                        text: 'Post Text Removed',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    for (x in request.responseJSON['errors']) {
                        new PNotify({
                            title: 'Error',
                            text: request.responseJSON['errors'][x],
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }

            });
        };

        let insertForm = () => {
            $( "#forminserter" ).append('<form class="form-horizontal form-label-left newforminserttext"  id="newforminserttext" method="post">\n' +
                '                                    <div style="font-size: 60px;cursor: pointer;width: 18px;margin-left: 50%;height: 57px;" onclick="deleteForm(this)">-</div>\n' +
                '                                    <br><br><br>\n' +
                '                                {{csrf_field()}}\n' +
                '                                <input type="hidden" name="post_id" value="{{$post->id}}">\n' +
                '                                <input type="hidden" name="image_genderID" value="1">\n' +
                '                                <div class="item form-group">\n' +
                '                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>\n' +
                '                                    </label>\n' +
                '                                    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '                                        <input id="title" class="form-control col-md-7 col-xs-12" name="title" placeholder="" required="required" type="text">\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '                                <div class="item form-group">\n' +
                '                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Text <span class="required">*</span>\n' +
                '                                    </label>\n' +
                '                                    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '                                        <textarea class="content-editor" name="postText" required="required" class="form-control col-md-7 col-xs-12"></textarea>\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '\n' +
                '                                <div class="item form-group">\n' +
                '                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Text <span class="required">*</span>\n' +
                '                                    </label>\n' +
                '                                    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '                                        <select name="leftOrRight" id="leftOrRight">\n' +
                '                                            <option value="right">right</option>\n' +
                '                                            <option value="left">left</option>\n' +
                '                                        </select>\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '                                    <div class="item form-group">\n' +
                '                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Show in breadcrumbs <span class="required">*</span>\n' +
                '                                        </label>\n' +
                '                                        <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '                                            <label>\n' +
                '                                                <input type="checkbox" name="breadcrumbs" value="2" class="js-switch"/>\n' +
                '                                            </label>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                <div class="item form-group">\n' +
                '                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="post_id">Images <span class="required">*</span>\n' +
                '                                    </label>\n' +
                '                                    <input type="hidden" name="image_genderID" value="1">\n' +
                '                                    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '                                        <label class="btn btn-danger btn-file">\n' +
                '                                            <i class="fa fa-file-o"></i> Choose File <input type="file" id="posttextfile" name="upl[]" style="display:none" multiple="" class="form-control col-md-7 col-xs-12">\n' +
                '                                            <div id="selectedFiles"></div>\n' +
                '                                        </label>\n' +
                '                                    </div>\n' +
                '                                    <div class="col-md-6 col-sm-6 col-xs-12">\n' +
                '\n' +
                '                                    </div>\n' +
                '\n' +
                '                                </div>\n' +
                '                                    <br><br><br><br>\n' +
                '                            </form>');
            tinyinicialization();


        };

        let deleteForm = (element) => {
            if($(".newforminserttext").length>1) {
                $(element).closest( "form" ).remove();
            }
        };

        let submitInsertText = () => {
            let i =0;
            while(i<$('.newforminserttext').length){
                console.log(i);
                insertText(i);
                i++;
            }
        }
      let insertText = (element) => {
          let formData = new FormData($('.newforminserttext')[element]);
          formData.append('csrfmiddlewaretoken', '{{ csrf_token() }}');
          console.log(formData)
          $.ajax({
              /* the route pointing to the post function */
              url: '{{URL::to('apanel/post/insertposttext')}}',
              type: 'POST',
              /* send the csrf-token and the input to the controller */
              data: formData,
              async: true,
              cache:false,
              contentType: false,
              processData: false,
              /* remind that 'data' is the response of the AjaxController */

              success: function (data) {
                  if(element == 0){
                      $('.newforminserttext')[element].reset();
                  }else {
                      $('.newforminserttext')[element].remove();
                  }
                  //refresh section
                  new PNotify({
                      title: 'Post ',
                      text: 'Post text Inserted ',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
              },

              error: function (request, status, error) {
                  for (x in request.responseJSON['errors']) {
                      new PNotify({
                          title: 'Error',
                          text: request.responseJSON['errors'][x],
                          type: 'error',
                          styling: 'bootstrap3'
                      });
                  }
              }

          });
       };

        let updateText = (element) => {
            let fullForm = $(element)[0];
            console.log(fullForm);
            let formData = new FormData(fullForm);
            formData.append('csrfmiddlewaretoken', '{{ csrf_token() }}');
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/post/updatenotetext')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Post ',
                        text: 'Post text updated ',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    for (x in request.responseJSON['errors']) {
                        new PNotify({
                            title: 'Error',
                            text: request.responseJSON['errors'][x],
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }

            });
        };

        function deletePost() {
                $(".wholecontent").hide();
                $(".x_content2").hide();
                $("#my-confirm-dialog").hide();
                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{URL::to('apanel/deletepostfromsection')}}',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: $("#updatenote").serialize(),
                    /* remind that 'data' is the response of the AjaxController */

                    success: function (data) {
                        //refresh section
                        new PNotify({
                            title: 'Post Deleted ',
                            text: 'Post Deleted <a href={{URL::to('apanel/'.$link)}}>go back </a>',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    },

                    error: function (request, status, error) {
                        for (x in request.responseJSON['errors']) {
                            new PNotify({
                                title: 'Error',
                                text: request.responseJSON['errors'][x],
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }
                    }

                });
            }
            document.getElementById("send").onclick=function (e) {

                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{URL::to('apanel/note/updatenote')}}',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: $("#updatenote").serialize(),
                    /* remind that 'data' is the response of the AjaxController */

                    success: function (data) {
                        //refresh section
                        new PNotify({
                            title: 'Post Updated ',
                            text: 'Post Updated <a class="btn btn-dark" href="{{URL::to('/')}}/{{$post->post_id}}" target="_blank">Preview</a>',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    },

                    error: function (request, status, error) {
                        for (x in request.responseJSON['errors']) {
                            new PNotify({
                                title: 'Error',
                                text: request.responseJSON['errors'][x],
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }
                    }

                });
            }

            function deleteimagefrompost(element) {
                var CSRF_TOKEN = "{{csrf_token()}}";
                $.ajax({
                    /* the route pointing to the post function */
                    url: $(element).data('deleteurl'),
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, imageID: $(element).data('imageid'), contentID:$(element).data('contentid'),relation_table:$(element).data('relationtable')},
                    /* remind that 'data' is the response of the AjaxController */

                    success: function (data) {
                        //refresh section
                        new PNotify({
                            title: 'Image Deleted ',
                            text: '',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                        refreshContent()
                    },

                    error: function (request, status, error) {
                        new PNotify({
                            title: 'Error',
                            text: 'There are only 1 image , so you can"t delete default image',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }

                });
            }

        function makedefaultimage(element) {
            var CSRF_TOKEN = "{{csrf_token()}}";
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/post/makedefaultimage')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, imageID: $(element).data('imageid'), contentID:$(element).data('contentid')},
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Image Set Default ',
                        text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent()>Refresh</button>',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    for (x in request.responseJSON['errors']) {
                        new PNotify({
                            title: 'Error',
                            text: request.responseJSON['errors'][x],
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }

            });
        }

        function changeorder(element) {
            var CSRF_TOKEN = "{{csrf_token()}}";
            $.ajax({
                /* the route pointing to the post function */
                url: $(element).data('uploadurl'),
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, imageID: $(element).data('imageid'), orderID:$(element).val()},
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Image Order Updated ',
                        text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent()>Refresh</button>',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    for (x in request.responseJSON['errors']) {
                        new PNotify({
                            title: 'Error',
                            text: request.responseJSON['errors'][x],
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }

            });
        }

        let changeorderArticle = (element) => {
            var CSRF_TOKEN = "{{csrf_token()}}";

            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/changeorderarticle')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, postID: $(element).data('postid'), articleID: $(element).data('articleid') ,orderID:$(element).val()},
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Order Updated in '+$(element).data('postname'),
                        text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent('+$(element).data('postid')+')>Refresh</button>',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    alert('error');
                    //get all error
                    // for (x in request.responseJSON['errors']) {
                    //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                    // }
                }

            });
        }

        let deleteArticleFromPost = (element) => {
            var CSRF_TOKEN = "{{csrf_token()}}";
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/deletearticlefrompost')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, postID: $(element).data('postid'), articleID: $(element).data('articleid') },
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Post Deleted',
                        text: 'Post '+$(element).data("postname")+' Drafted Successfully',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    $( "#post-"+$(element).data('postid') ).load(window.location.href + " #post-"+$(element).data('postid') );

                },

                error: function (request, status, error) {
                    alert('error');
                    //get all error
                    // for (x in request.responseJSON['errors']) {
                    //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                    // }
                }

            });
        }
        function refreshContent() {
            $( ".x_content2").load(window.location.href + " .x_content2");
        }

        $(function(){
            $('#posttextfile').change(function(){
                var names = [];
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    names.push('<li>' + $(this).get(0).files[i].name + '</li>');
                }
                $("#selectedFiles").html(names);
            });
        });
    </script>
    <!-- /page content -->
@endsection
