@extends('dashboard_layouts.master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3> {{$title}} </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="row" id="content">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1>Drafted Posts</h1>
                        <h2>Counted: {{count($posts)}}</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="row">
                            @foreach($posts as $post)
                            <div class="col-md-55">
                                <div class="thumbnail">
                                    <div class="image view view-first">
                                        <img style="width: 100%; display: block;" src="{{URL::to($post->cover_image['image']['image'])}}" alt="image" />
                                        <div class="mask">
                                            <select data-postname="{{$post->title}}" data-postid="{{$post->id}}" style="margin-top:5px" onchange="publishPost(this)">
                                                <option value="">Choose Section</option>
                                                @foreach($sections as $section)
                                                <option value="{{$section->section_id}}">{{$section->section_title}}</option>
                                                @endforeach
                                            </select>

                                            <select data-postname="{{$post->title}}" data-postid="{{$post->id}}" style="margin-top:5px" onchange="publishArticle(this)">
                                                <option value="">Choose Post</option>
                                                @foreach($sections_posts as $sections_post)
                                                    <option value="{{$sections_post->id}}">{{$sections_post->title}}</option>
                                                @endforeach
                                            </select>
                                            <div class="tools tools-bottom">
                                                <a href="{{URL::to('apanel/preview/'.$post->slug)}}" target="_blank"><i class="fa fa-link"></i></a>
                                                <a href="{{URL::to('apanel/post/')}}/{{$post->slug}}"><i class="fa fa-pencil"></i></a>
                                                <a href="#" data-postid="{{$post->id}}" onclick="return confirm('Are you sure you want to delete this post , it cant be recovered ?')? deletepostfromdrafts(this) : '' "><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <a href="{{URL::to('apanel/post/')}}/{{$post->slug}}"> <p>{{$post->title}}<br>{{$post->description}}</p></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<script type="text/javascript">
    $("form").submit((e) => {
        e.preventDefault();
    });
    function deletepostfromdrafts(element){
        var CSRF_TOKEN = "{{csrf_token()}}";
        $.ajax({
            /* the route pointing to the post function */
            url: '{{URL::to('apanel/deletepostfromdrafts')}}',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, postID: $(element).data('postid') },
            /* remind that 'data' is the response of the AjaxController */
            success: function (data) {
                //refresh section
                //refresh section
                new PNotify({
                    title: 'Draft successfully deleted',
                    text: 'content refreshed',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                refreshContent();
            },

            error: function (request, status, error) {
                for (x in request.responseJSON['errors']) {
                    new PNotify({
                        title: 'Error',
                        text: request.responseJSON['errors'][x],
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            }

        });
    }

    function publishPost(element) {
        var CSRF_TOKEN = "{{csrf_token()}}";
        let orderid = prompt("Please enter order id , default position will be in the end of the section:", 9999);
        $.ajax({
            /* the route pointing to the post function */
            url: '{{URL::to('apanel/publishpost')}}',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, postID: $(element).data('postid'), sectionID: $(element).val() ,orderID:orderid},
            /* remind that 'data' is the response of the AjaxController */

            success: function (data) {
                //refresh section
                new PNotify({
                    title: $(element).data("postname")+' Draft successfully published',
                    text: 'Refresh content <button class="btn btn-dark" onclick="refreshContent()")>Refresh</button>',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            },

            error: function (request, status, error) {
                for (x in request.responseJSON['errors']) {
                    new PNotify({
                        title: 'Error',
                        text: request.responseJSON['errors'][x],
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            }

        });
    }

    function publishArticle(element) {
        var CSRF_TOKEN = "{{csrf_token()}}";
        let orderid = prompt("Please enter order id , default position will be in the end of the section:", 9999);
        $.ajax({
            /* the route pointing to the post function */
            url: '{{URL::to('apanel/publisharticle')}}',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, postID: $(element).val(), articleID: $(element).data('postid') ,orderID:orderid},
            /* remind that 'data' is the response of the AjaxController */

            success: function (data) {
                //refresh section
                new PNotify({
                    title: $(element).data("postname")+' Draft successfully published',
                    text: 'Refresh content <button class="btn btn-dark" onclick="refreshContent()")>Refresh</button>',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            },

            error: function (request, status, error) {
                for (x in request.responseJSON['errors']) {
                    new PNotify({
                        title: 'Error',
                        text: request.responseJSON['errors'][x],
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            }

        });
    }
    function refreshContent() {
        $( "#content").load(window.location.href + " #content");
    }
</script>
@endsection