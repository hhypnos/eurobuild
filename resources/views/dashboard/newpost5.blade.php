@extends('dashboard_layouts.master')
@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="wholecontent">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{translate('New Service',session('languageID'))}}</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{translate('Insert New Service',session('languageID'))}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <style>
                                .checkbox{
                                    display: inline-block;
                                }
                            </style>
                            <form class="form-horizontal form-label-left" id="insertpost" method="post" action="{{URL::to('apanel/post/insertpost')}}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                @php($draft=true)
                                @php($link = "drafts")

                                <br><br>
                                <span class="section">Services</span>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Language">Language <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="language" class="form-control col-md-7 col-xs-12" name="default_lang" required="required" >
                                            <option value="2">English</option>
                                            <option value="1">ქართული</option>
                                            <option value="3">Руский</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="title" class="form-control col-md-7 col-xs-12" name="title" placeholder="Title" value="{{ old('title') }}" required="required" type="text">
                                    </div>
                                </div>

                                <input class="form-control col-md-7 col-xs-12" name="type_id" placeholder="type id" value="2" required="required" type="hidden">
                                <input class="form-control col-md-7 col-xs-12" name="relation_table" placeholder="type id" value="App\Models\Service" required="required" type="hidden">
                                <br><br><br>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Text <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="content-editor3" type="text" name="text" required="required" class="content-editor form-control col-md-7 col-xs-12">{{ old('description') }}</textarea>
                                    </div>
                                </div>
                                <br><br><br>
                                <br><br><br>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="text">Description <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="content-editor2" type="text" name="description" required="required" class="content-editor form-control col-md-7 col-xs-12">{{ old('description') }}</textarea>
                                    </div>
                                </div>
                                <br><br><br>


                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="slug">Images <span class="required">*</span>
                                    </label>
                                    <input type="hidden" name="image_genderID" value="1">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label class="btn btn-danger btn-file">
                                            <i class="fa fa-file-o"></i> Choose File <input type="file" id="files" name="upl" style="display:none"  class="form-control col-md-7 col-xs-12">
                                            <div id="selectedFiles"></div>
                                        </label>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                    </div>

                                </div>


                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button  id="cancel" onclick="event.preventDefault();(function(){window.history.back();})();" class="btn btn-primary">Cancel</button>
                                        <button id="send" type="submit" class="btn btn-success">Create Post</button>
                                        {{--<button id="delete" onclick="event.preventDefault();(function(){$('#my-confirm-dialog').show(); })();" class="btn btn-danger">Delete</button>--}}
                                    </div>

                                </div>
                                {{--CUSTOM CSS FOR THIS FORM --}}
                                <style>
                                    #upload{
                                        font-family:'PT Sans Narrow', sans-serif;
                                        background-color:#fff;
                                        background-image:-webkit-linear-gradient(top, #fff, #fff);
                                        background-image:-moz-linear-gradient(top, #fff, #fff);
                                        background-image:linear-gradient(top, #fff, #fff);
                                        /*width:250px;*/
                                        padding:0;
                                        border-radius:0;
                                        /*margin:200px auto 100px;*/
                                        box-shadow: none;
                                    }
                                </style>


                            </form>

                        </div>
                        <div class="x_content2">

                            <div class="row">
                                {{--@foreach($post->all_images as $image)--}}
                                {{--<div class="col-md-55">--}}
                                {{--<div class="thumbnail">--}}
                                {{--<div class="image view view-first" style="height: 100%;">--}}
                                {{--<img style="width: 100%; display: block;" src="{{URL::to('/'.$image['image']['image'])}}" alt="image" />--}}
                                {{--<div class="mask">--}}
                                {{--<input type="number" data-imageid="{{$image->id}}"  onchange="changeorder(this)" style="color:black;width:15%; margin-top:5px" value="{{$image->order_id}}" >--}}
                                {{--<div class="tools tools-bottom">--}}
                                {{--<a href="{{URL::to('/'.$image->image)}}" target="_blank"><i class="fa fa-link"></i></a>--}}
                                {{--<a href="#" ><i class="fa fa-star" data-imageid="{{$image->id}}"  data-contentid="{{$post->post_id}}" onclick="makedefaultimage(this)" style="{{$image->defaultOrNot == 2 ? 'color:red':'color:white'}}"></i></a>--}}
                                {{--<a href="#"><i data-imageid="{{$image->id}}" data-contentid="{{$post->post_id}}" onclick="return confirm('Are you sure you want to delete image ?')? deleteimagefrompost(this) : '' " class="fa fa-times"></i></a>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--@endforeach--}}

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{--<div id="my-confirm-dialog" class="dialog-overlay">--}}

    {{--<div class="dialog-card">--}}

    {{--<div class="dialog-question-sign"><i class="fa fa-question"></i></div>--}}

    {{--<div class="dialog-info">--}}

    {{--<h5>Are you sure?</h5>--}}
    {{--<p>Post Can't be restored</p>--}}

    {{--<button class="dialog-confirm-button" onclick="deletePost()">Yes</button>--}}
    {{--<button class="dialog-reject-button" onclick='(function(){$("#my-confirm-dialog").hide(); })();'>No</button>--}}

    {{--</div>--}}

    {{--</div>--}}

    {{--</div>--}}
    @include('dashboard_layouts.errors')

    <script>
        $("form").submit((e) => {
            e.preventDefault();
            let formData = new FormData($('form')[0]);
            formData.append('csrfmiddlewaretoken', '{{ csrf_token() }}');
            console.log(formData)
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/product/insertservices')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Post ',
                        text: 'Post Inserted ',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    for (x in request.responseJSON['errors']) {
                        new PNotify({
                            title: 'Error',
                            text: request.responseJSON['errors'][x],
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                }

            });
        });
        $(function(){
            $('#files').change(function(){
                var names = [];
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    names.push('<li>' + $(this).get(0).files[i].name + '</li>');
                }
                $("#selectedFiles").html(names);
            });
            $('#files2').change(function(){
                var names = [];
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    names.push('<li>' + $(this).get(0).files[i].name + '</li>');
                }
                $("#selectedFiles2").html(names);
            });
        });
    </script>
    <!-- /page content -->
@endsection
