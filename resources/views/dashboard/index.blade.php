@extends('dashboard_layouts.master')
@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3> {{$title}} </h3>
                </div>

                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
    <!-- /page content -->
    <div class="clearfix"></div>
    <div class="row" id="section-1">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h1>{{$section_title}}</h1>

                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">

                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
    <div class="x_content">

        <div class="row">
            @foreach($galleries as $gallery)
                <div class="col-md-55">
                    <div class="thumbnail">
                        <div class="image view view-first">
                            <a href="#"><img style="width: 100%; display: block;" src="{{$gallery->image}}" alt="image" /></a>
                            <div class="mask">
                                <input type="number" data-sectionname="work" data-postid="{{$gallery->id}}" data-sectionid="1" onchange="changeorder(this)" style="color:black;width:15%; margin-top:5px" value="{{$gallery->order_id}}" >
                                <div class="tools tools-bottom">
                                    <a href="#" data-postid="{{$gallery->id}}" data-sectionid="1" onclick="return confirm('Are you sure you want to delete this post from section ?')? deletepostfromsection(this) : '' "><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
            </div>
        </div>
    </div>
        </div>
    </div>
    <form id="upload" method="post" action="{{URL::to('apanel/post/uploadgallery')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div id="drop">
            Upload Gallery

            <a>Browse</a>

            <input type="file" name="upl"  />
        </div>

        <ul>
            <!-- The file uploads will be shown here -->
        </ul>
    </form>
    <!-- /page content -->
    <script type="text/javascript">
        $("form").submit((e) => {
            e.preventDefault();
        });
        function deletepostfromsection(element){
            var CSRF_TOKEN = "{{csrf_token()}}";
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/deletegalleryfromsection')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, postID: $(element).data('postid'), sectionID: $(element).data('sectionid') },
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Post Deleted',
                        text: 'Post '+$(element).data("postname")+' Drafted Successfully',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    $( "#section-"+$(element).data('sectionid') ).load(window.location.href + " #section-"+$(element).data('sectionid') );

                },

                error: function (request, status, error) {
                    alert('error');
                    //get all error
                    // for (x in request.responseJSON['errors']) {
                    //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                    // }
                }

            });
        }

        function changeorder(element) {
            var CSRF_TOKEN = "{{csrf_token()}}";
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/changeordergallery')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, postID: $(element).data('postid'), sectionID: $(element).data('sectionid') ,orderID:$(element).val()},
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Order Updated in '+$(element).data('sectionname'),
                        text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent('+$(element).data('sectionid')+')>Refresh</button>',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    alert('error');
                    //get all error
                    // for (x in request.responseJSON['errors']) {
                    //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                    // }
                }

            });
        }



        function applystyle(element) {
            var CSRF_TOKEN = "{{csrf_token()}}";
            $.ajax({
                /* the route pointing to the post function */
                url: '{{URL::to('apanel/sectionapplystyle')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, sectionstyleID: $(element).data('sectionstyleid'), sectionID: $(element).data('sectionid')},
                /* remind that 'data' is the response of the AjaxController */

                success: function (data) {
                    //refresh section
                    new PNotify({
                        title: 'Style changed '+$(element).data('sectionname'),
                        text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent('+$(element).data('sectionid')+')>Refresh</button>',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                },

                error: function (request, status, error) {
                    alert('error');
                    //get all error
                    // for (x in request.responseJSON['errors']) {
                    //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                    // }
                }

            });
        }

        function refreshContent(sectionid) {
            $( "#section-"+sectionid).load(window.location.href + " #section-"+sectionid);
        }
    </script>
@endsection

