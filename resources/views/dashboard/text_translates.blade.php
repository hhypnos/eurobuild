@extends('dashboard_layouts.master')
@section('content')
    <style>
        .grid-object-container {
            width:100%;
            display: flex;
            flex-wrap: wrap;
        }
        .grid-object-container .grid-object-x2{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:50%;
            float:left;
            padding:20px;
        }
        .grid-object-container .grid-object-x3{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:33.33%;
            float:left;
            padding:20px;
        }

        .grid-object-container .grid-object-x4{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:25%;
            float:left;
            padding:20px;
        }
        .grid-object-container .grid-object-x5{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:20%;
            float:left;
            padding:20px;
        }
        .grid-object-container .grid-object-x6{
            box-shadow: inset 0 0 0 1px coral;
            height:auto;
            width:16.66%;
            float:left;
            padding:20px;
        }

        @media(max-width:800px){
            .grid-object-container .grid-object-x2{
                width:50%;
            }
            .grid-object-container .grid-object-x3{
                width:50%;
            }
            .grid-object-container .grid-object-x4{
                width:50%;
            }
            .grid-object-container .grid-object-x5{
                width:50%;
            }
            .grid-object-container .grid-object-x6{
                width:50%;
            }
        }
        @media(max-width:500px){
            .grid-object-container .grid-object-x2{
                width:100%;
            }
            .grid-object-container .grid-object-x3{
                width:100%;
            }
            .grid-object-container .grid-object-x4{
                width:100%;
            }
            .grid-object-container .grid-object-x5{
                width:100%;
            }
            .grid-object-container .grid-object-x6{
                width:100%;
            }
        }
    </style>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="wholecontent">
            <div class="page-title">
                <div class="title_left">
                    <h3>{{$post['title']}}</h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{$post['subtitle']}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <style>
                                .checkbox{
                                    display: inline-block;
                                }
                            </style>
                            <form class="form-horizontal form-label-left" id="updatepost" method="post" >
                                {{csrf_field()}}
                                <div class="checkbox">
                                  <ul>
                                      <li><a href="{{route('translated_text',array($post_original->post_texts_id,1))}}">ENGLISH</a></li>
                                      <li> <a href="{{route('translated_text',array($post_original->post_texts_id,2))}}">GEORGIAN</a></li>
                                      <li><a href="{{route('translated_text',array($post_original->post_texts_id,3))}}">RUSSIAN</a></li>
                                  </ul>
                                </div>

                                 <br><br>
                                <span class="section">Data</span>
                                        <input type="hidden" name="post_texts_id" value="{{$post_original['id']}}">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="title" class="form-control col-md-7 col-xs-12" name="title" placeholder="" required="required" value="{{$post['title']}}" type="text">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="metaDescription">Text <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="content-editor" class="content-editor" name="postText" required="required" class="form-control col-md-7 col-xs-12">{!! $post['text']!!}</textarea>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button id="send" type="submit"class="btn btn-warning">translate</button>
                                    </div>

                                </div>
                            </form>
                            <br><br><br><br><br>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="my-confirm-dialog" class="dialog-overlay">

        <div class="dialog-card">

            <div class="dialog-question-sign"><i class="fa fa-question"></i></div>

            <div class="dialog-info">

                <h5>Are you sure?</h5>
                <p>Post Can't be restored</p>

                <button class="dialog-confirm-button" onclick="deletePost()">Yes</button>
                <button class="dialog-reject-button" onclick='(function(){$("#my-confirm-dialog").hide(); })();'>No</button>

            </div>

        </div>

    </div>

    <script>
        $("form").submit((e) => {
            e.preventDefault();
        });



            document.getElementById("send").onclick=function (e) {

                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{route('update_text_translates',array($post_original->post_texts_id,Request::segment(4)))}}',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: $("#updatepost").serialize(),
                    /* remind that 'data' is the response of the AjaxController */

                    success: function (data) {
                        //refresh section
                        new PNotify({
                            title: 'Post Updated ',
                            text: 'Post Updated <a class="btn btn-dark" href="{{URL::to('/')}}/{{$post['slug']}}" target="_blank">Preview</a>',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    },

                    error: function (request, status, error) {
                        for (x in request.responseJSON['errors']) {
                            new PNotify({
                                title: 'Error',
                                text: request.responseJSON['errors'][x],
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                        }
                    }

                });
            }





        function refreshContent() {
            $( ".x_content2").load(window.location.href + " .x_content2");
        }

        $(function(){
            $('#posttextfile').change(function(){
                var names = [];
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    names.push('<li>' + $(this).get(0).files[i].name + '</li>');
                }
                $("#selectedFiles").html(names);
            });
        });
    </script>
    <!-- /page content -->
@endsection