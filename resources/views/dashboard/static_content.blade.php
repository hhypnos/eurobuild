@extends('dashboard_layouts.master')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3> {{$title}} </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="row" id="section-1">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h1>{{$section_title}}</h1>

                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">

                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="row">
                            @foreach($posts as $post)
                                <p>Page: {{$post->page}}</p>
                                <form method="POST" action="{{URL::to('apanel/section-content')}}">
                                    {{csrf_field()}}
                                <textarea name="content" class="form-control col-md-7 col-xs-12">{{$post->content}}</textarea>
                                <input type="hidden" name="id" value="{{$post->id}}">
                                <input type="hidden" name="lang_id" value="{{$post->lang_id}}">
                                <input type="hidden" name="key" value="{{$post->key}}">
                                <input type="hidden" name="page" value="{{$post->page}}">
                                <input type="submit" value="submit">
                            </form>
                            @php
                            $eng = \App\Models\Static_content::where('key',$post->key)->where('page',$post->page)->where('lang_id',2)->first();
                            @endphp
                                <hr>
                                <hr>
                            English Version
                                <form method="POST" action="{{URL::to('apanel/section-content')}}">
                                    {{csrf_field()}}
                                    <textarea name="content" class="form-control col-md-7 col-xs-12">{{!empty($eng) ? $eng['content']:''}}</textarea>
                                    <input type="hidden" name="id" value="{{!empty($eng) ? $eng['id']:''}}">
                                    <input type="hidden" name="lang_id" value="2">
                                    <input type="hidden" name="key" value="{{$post->key}}">
                                    <input type="hidden" name="page" value="{{$post->page}}">
                                    <input type="submit" value="submit">
                                </form>
                                @php
                                    $rus = \App\Models\Static_content::where('key',$post->key)->where('page',$post->page)->where('lang_id',3)->first();
                                @endphp
                                <hr>
                                <hr>
                                Russian Version
                                <form method="POST" action="{{URL::to('apanel/section-content')}}">
                                    {{csrf_field()}}
                                    <textarea name="content" class="form-control col-md-7 col-xs-12">{{!empty($rus) ? $rus['content']:''}}</textarea>
                                    <input type="hidden" name="id" value="{{!empty($rus) ? $rus['id']:''}}">
                                    <input type="hidden" name="lang_id" value="3">
                                    <input type="hidden" name="key" value="{{$post->key}}">
                                    <input type="hidden" name="page" value="{{$post->page}}">
                                    <input type="submit" value="submit">
                                </form>
                                <hr>
                                <hr>
                                <hr>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<script type="text/javascript">

    function deletepostfromsection(element){
        var CSRF_TOKEN = "{{csrf_token()}}";
        $.ajax({
            /* the route pointing to the post function */
            url: '{{URL::to('apanel/deletepostfromsection')}}',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, postID: $(element).data('postid'), sectionID: $(element).data('sectionid') },
            /* remind that 'data' is the response of the AjaxController */
            success: function (data) {
                //refresh section
                new PNotify({
                    title: 'Post Deleted',
                    text: 'Post '+$(element).data("postname")+' Drafted Successfully',
                    type: 'success',
                    styling: 'bootstrap3'
                });
                $( "#section-"+$(element).data('sectionid') ).load(window.location.href + " #section-"+$(element).data('sectionid') );

            },

            error: function (request, status, error) {
                alert('error');
                //get all error
                // for (x in request.responseJSON['errors']) {
                //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                // }
            }

        });
    }

    function changeorder(element) {
        var CSRF_TOKEN = "{{csrf_token()}}";
        $.ajax({
            /* the route pointing to the post function */
            url: '{{URL::to('apanel/changeorder')}}',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, postID: $(element).data('postid'), sectionID: $(element).data('sectionid') ,orderID:$(element).val()},
            /* remind that 'data' is the response of the AjaxController */

            success: function (data) {
                //refresh section
                new PNotify({
                    title: 'Order Updated in '+$(element).data('sectionname'),
                    text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent('+$(element).data('sectionid')+')>Refresh</button>',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            },

            error: function (request, status, error) {
                alert('error');
                //get all error
                // for (x in request.responseJSON['errors']) {
                //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                // }
            }

        });
    }



    function applystyle(element) {
        var CSRF_TOKEN = "{{csrf_token()}}";
        $.ajax({
            /* the route pointing to the post function */
            url: '{{URL::to('apanel/sectionapplystyle')}}',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, sectionstyleID: $(element).data('sectionstyleid'), sectionID: $(element).data('sectionid')},
            /* remind that 'data' is the response of the AjaxController */

            success: function (data) {
                //refresh section
                new PNotify({
                    title: 'Style changed '+$(element).data('sectionname'),
                    text: 'Refresh Section <button class="btn btn-dark" onclick=refreshContent('+$(element).data('sectionid')+')>Refresh</button>',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            },

            error: function (request, status, error) {
                alert('error');
                //get all error
                // for (x in request.responseJSON['errors']) {
                //     document.getElementById("bookBTN").innerHTML += request.responseJSON['errors'][x] + "<br>";
                // }
            }

        });
    }

    function refreshContent(sectionid) {
        $( "#section-"+sectionid).load(window.location.href + " #section-"+sectionid);
    }
</script>
@endsection
