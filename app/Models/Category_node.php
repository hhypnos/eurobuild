<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category_node extends Model
{
    use SoftDeletes;

    public function category()
    {
        return $this->morphTo();
    }
    public function full_category()
    {
        return $this->belongsTo('\App\Category','category_id','id');
    }
}
