<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Static_content extends Model
{
    use SoftDeletes;

    public function contentType(){
        return $this->hasOne('\App\Models\Static_content_type','id','type_id');
    }
}
