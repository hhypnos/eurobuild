<?php
/**
 * Subscribers
 *
 * @param $email
 */
function subscribers($email)
{
    if(!DB::table('subscribes')->where('email',$email)->first()){
        DB::table('subscribes')
            ->updateOrInsert(
                ['email' => $email]
            );
        return 1;
    }
}
/**
* translate static texts
*
* @param $word
* @param $lang
*/
function translate($word, $lang)
{
    if(DB::table('translates')->where('lang_id', $lang)->where('origin',$word)->first()){
        return DB::table('translates')->where('lang_id', $lang)->where('origin',$word)->first()->translated;
    }else{
        return $word;
    }
}
