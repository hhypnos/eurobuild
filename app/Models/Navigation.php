<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Navigation extends Model
{
    use SoftDeletes;

    public function fetchRecursively($parent_id,$orderId,$lang_id){
        $result = [];
        foreach($this->orderBy('order_id',$orderId)->where('lang_id',$lang_id)->where('parent_id',$parent_id)->get() as $group) {
            $group['children'] = $this->fetchRecursively($group->id,$orderId,$lang_id);
            $result[] = $group;
        }
        return $result;
    }
}
