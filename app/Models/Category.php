<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    public function fetchRecursively($parent_id,$orderId){
        $result = [];
        foreach($this->orderBy('order_id',$orderId)->where('parent_id',$parent_id)->get() as $group) {
            $group['children'] = $this->fetchRecursively($group->id,$orderId);
            $result[] = $group;
        }
        return $result;
    }
}
