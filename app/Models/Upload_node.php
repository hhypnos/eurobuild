<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Upload_node extends Model
{
    use SoftDeletes;

    public function images()
    {
        return $this->morphTo();
    }
    public function full_files()
    {
        return $this->belongsTo('\App\Models\Upload','file_id','id');
    }
}
