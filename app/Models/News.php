<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    public function images(){
        return $this->morphMany('\App\Models\Upload_node', 'images','relation_table','data_id','post_id')->where('type_id',1);
    }

    public function covers(){
        return $this->morphMany('\App\Models\Upload_node', 'images','relation_table','data_id','post_id')->where('type_id',2);
    }
}
