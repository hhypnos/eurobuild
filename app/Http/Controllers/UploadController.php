<?php

namespace App\Http\Controllers;

use App\Models\Upload;
use App\Models\Upload_node;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UploadController extends Controller
{
    public function uploadFile(Request $request, $data_id = 0, $directory = 'uploads')
    {
        foreach ($request->file() as $file) {

            if ($request->type_id == 2) {
                foreach ($file as $f) {
                        $request->type_id = 2;
                    $this->addAndAssign($request, $f, $data_id, $directory);
                }
            }
  
	    if ($request->type_id == 1) {
		     foreach ($file as $f) {
                          $request->type_id = 1;
                      $this->addAndAssign($request, $f, $data_id, $directory);
                 }

                }
                $this->addAndAssign($request, $file, $data_id, $directory);
            }
        
        return response()->json(['status' => 'success', 'result' => [
            'data' => $request->file()
        ]
        ]);
    }


    public function addAndAssign(Request $request, $file, $data_id, $directory)
    {
        $uploadFile = $this->addUpload($file, $directory);
        $this->assignUpload((object)['id' => $data_id,
            'file_id' => $uploadFile->getData()->result->data->id,
            'type_id' => $request->type_id,
            'relation_table' => $request->relation_table,
            'is_private' => $request->is_private,
            'is_default' => $request->is_default,
            'order_id' => $request->order_id]);
        $file->move($directory, $uploadFile->getData()->result->data->name);
    }

    public function addUpload($data, $directory = 'uploads')
    {
        $validator = Validator::make(
            [
                'parent_id' => request('parent_id'),
                'size' => 1,
//                'source_id' => $data->source_id,
//                'quality_id' => $data->quality_id,
                'lang_id' => request('lang_id'),
            ],
            [
                'parent_id' => ['nullable', 'numeric'],
                'size' => ['nullable', 'numeric'],
                'source_id' => ['nullable', 'numeric'],
                'quality_id' => ['nullable', 'numeric'],
                'lang_id' => ['nullable', 'numeric', 'exists:languages,lang_id'],
            ]
        );
        if ($validator->fails()) {
            return response(['status' => 'error', 'result' => ['message' => $validator->messages()]], 400);
        }

        $upload = new Upload();
        $upload->parent_id = request('parent_id') ? request('parent_id') : 0;
        $upload->uploaded_by = 1;//auth()->user()->id
        $upload->name = time() . '-' . $data->getClientOriginalName();
        $upload->name_original = $data->getClientOriginalName();
//        $upload->description = $data->description;
//        $upload->full_description = $data->full_description;
//        $upload->encoding = $data->encoding;
        $upload->size = $data->getSize();
//        $upload->metadata = $data->metadata;
        $upload->mime_type = $data->getClientMimeType();
//        $upload->content_type = $data->content_type;
        $upload->path = $directory . '/' . $upload->name;
        $upload->full_path = ENV('APP_URL') . '/' . $directory . '/' . $upload->name;
//        $upload->source_id = $data->source_id;
//        $upload->quality_id = $data->quality_id;
        $upload->lang_id = request('lang_id') ? request('lang_id') : 1;
        $upload->save();
        return response()->json(['status' => 'success', 'result' => [
            'data' => $upload
        ]
        ]);
    }

    public function assignUpload($data)
    {
        $validator = Validator::make(
            [
                'data_id' => $data->id,
                'type_id' => $data->type_id,
                'relation_table' => $data->relation_table,
                'is_private' => $data->is_private,
                'is_default' => $data->is_default,
                'order_id' => $data->order_id,
            ],
            [
                'data_id' => ['required', 'numeric'],
                'type_id' => ['nullable', 'numeric'],
                'relation_table' => ['required'],
                'is_private' => ['nullable', 'numeric'],
                'is_default' => ['nullable', 'numeric'],
                'order_id' => ['nullable', 'numeric'],
            ]
        );
        if ($validator->fails()) {
            return response(['status' => 'error', 'result' => ['message' => $validator->messages()]], 400);
        }
        $uploadNode = new Upload_node();
        $uploadNode->data_id = $data->id;
        $uploadNode->file_id = $data->file_id;
        $uploadNode->type_id = $data->type_id ? $data->type_id : 1;
        $uploadNode->relation_table = $data->relation_table;
        $uploadNode->is_private = $data->is_private ? $data->is_private : 0;
        $uploadNode->is_default = $data->is_default ? $data->is_default : 0;
        $uploadNode->order_id = $data->order_id ? $data->order_id : 0;
        $uploadNode->save();
        return response()->json(['status' => 'success', 'result' => [
            'data' => $uploadNode
        ]
        ]);
    }

    public function detachUpload($id)
    {
        $validator = Validator::make(
            [
                'id' => $id,
            ],
            [
                'id' => ['required', 'numeric', 'exists:upload_nodes,id'],
            ]
        );
        if ($validator->fails()) {
            return response(['status' => 'error', 'result' => ['message' => $validator->messages()]], 400);
        }
        $uploadNode = Upload_node::findOrFail($id);
        $uploadNode->delete();
        return response()->json(['status' => 'success', 'result' => [
            'data' => $uploadNode
        ]
        ]);
    }

    public function deleteUpload($id)
    {
        $validator = Validator::make(
            [
                'id' => $id,
            ],
            [
                'id' => ['required', 'numeric', 'exists:services,service_id'],
            ]
        );
        if ($validator->fails()) {
            return response(['status' => 'error', 'result' => ['message' => $validator->messages()]], 400);
        }
        $upload = Upload::where('service_id', $id)->delete();
        return response()->json(['status' => 'success', 'result' => [
            'data' => $upload
        ]
        ]);
    }
}
