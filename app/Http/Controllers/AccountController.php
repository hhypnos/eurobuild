<?php

namespace App\Http\Controllers;

use App\Category_node;
use App\Models\Gallery;
use App\Models\PageGallery;
use App\Models\Static_content;
use App\Post;
use App\Post_section;
use App\Post_text;
use App\Post_texts_image;
use App\Published_text;
use App\Models\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    private $language;

    public function __construct()
    {
        if (session('languageID') === null) {
            session(['languageID' => 1]);
        }
        $this->language = session('languageID');
    }

    public function index()
    {
        $galleries = Gallery::get();
        return view('dashboard.index', compact('galleries'))->with(['title' => 'A Panel', 'section_title' => 'Gallery']);
    }
    public function home_page_gallery()
    {
        $galleries = PageGallery::get();
        return view('dashboard.pagegallery', compact('galleries'))->with(['title' => 'A Panel', 'section_title' => 'Gallery']);
    }

    public function login()
    {
        return view('dashboard.login')->with(['title' => 'A Panel']);
    }

    //show all posts with sections & articles in it...
    public function section_work_content()
    {
        $posts = \App\Models\News::with(['images.full_files', 'covers.full_files'])->orderBy('order_id', 'asc')->where('lang_id', 1)->get();
        return view('dashboard.section_content', compact('posts'))->with(['title' => 'A Panel', 'section_title' => 'News']);
    }

    public function section_static_content()
    {
        $posts = \App\Models\Static_content::where('lang_id', 1)->orderBy('page','asc')->get();
        return view('dashboard.static_content', compact('posts'))->with(['title' => 'A Panel', 'section_title' => 'News']);
    }

    public function translate_static_content()
    {
        $static_content = \App\Models\Static_content::where('id', request('id'))->first();
        if (empty($static_content)) {
            $static_content = new \App\Models\Static_content();
        }
        $static_content->lang_id = request('lang_id');
        $static_content->key = request('key');
        $static_content->page = request('page');
        $static_content->content = request('content');
        $static_content->save();
        return redirect(\URL::to('apanel/section-content'));
    }

    public function section_products_content()
    {
        $posts = \App\Models\Product::with(['images.full_files', 'covers.full_files'])->orderBy('order_id', 'asc')->where('lang_id', 1)->get();
        return view('dashboard.products', compact('posts'))->with(['title' => 'A Panel', 'section_title' => 'products']);
    }

    public function section_notes_content()
    {
        $posts = \App\Models\Note::with(['images.full_files'])->orderBy('order_id', 'asc')->where('lang_id', 1)->get();
        return view('dashboard.notes', compact('posts'))->with(['title' => 'A Panel', 'section_title' => 'Notes']);
    }
    public function section_categories_content()
    {
        $posts = \App\Models\Navigation::where('parent_id','!=',0)->get();
        return view('dashboard.categories', compact('posts'))->with(['title' => 'A Panel', 'section_title' => 'Notes']);
    }

    public function section_vacancies_content()
    {
        $posts = \App\Models\Vacancy::orderBy('order_id', 'asc')->where('lang_id', 1)->get();
        return view('dashboard.vacancies', compact('posts'))->with(['title' => 'A Panel', 'section_title' => 'Vacancies']);
    }

    public function section_services_content()
    {
        $posts = \App\Models\Service::orderBy('order_id', 'asc')->where('lang_id', 1)->get();
        return view('dashboard.services', compact('posts'))->with(['title' => 'A Panel', 'section_title' => 'Services']);
    }

    public function section_orders_content()
    {
        $posts = \App\Models\Order::orderBy('created_at', 'desc')->get();
        return view('dashboard.orders', compact('posts'))->with(['title' => 'A Panel', 'section_title' => 'Orders']);
    }

    public function section_news_content()
    {
        $posts = \App\Post::with(['categories.full_category', 'tags.full_tag', 'images.full_files'])->orderBy('order_id', 'asc')->where('type_id', 3)->where('lang_id', 1)->get();
        return view('dashboard.section_content', compact('posts'))->with(['title' => 'A Panel', 'section_title' => 'news']);
    }

    public function section_apply_style()
    {
        $this->validate(request(),
            [
                'sectionstyleID' => 'required|exists:section_styles,section_style_id',
                'sectionID' => 'required'
            ]
        );

        $section = \App\Section::where('section_id', request('sectionID'))->firstOrFail();
        $section->section_style_id = request('sectionstyleID');
        $section->save();

        return response()->json(['info' => 'style changed succussfully',]);

    }

    public function article_content()
    {
        $posts = \App\Post::orderBy('created_at', 'asc')->get();
        return view('dashboard.article_content', compact('posts'))->with(['title' => 'A Panel']);
    }

    public function changeorder()
    {
        $this->validate(request(),
            [
                'postID' => 'required',
                'orderID' => 'required'
            ]
        );
        $post_section = \App\Models\News::where('post_id', request('postID'))->update(['order_id' => request('orderID')]);
        return response()->json(['info' => 'order changed succussfully', 'postID' => request('postID')]);
    }

    public function changeorder_products()
    {
        $this->validate(request(),
            [
                'postID' => 'required',
                'orderID' => 'required'
            ]
        );
        $post_section = \App\Models\Product::where('post_id', request('postID'))->update(['order_id' => request('orderID')]);
        return response()->json(['info' => 'order changed succussfully', 'postID' => request('postID')]);
    }

    public function changeorder_services()
    {
        $this->validate(request(),
            [
                'postID' => 'required',
                'orderID' => 'required'
            ]
        );
        $post_section = \App\Models\Service::where('post_id', request('postID'))->update(['order_id' => request('orderID')]);
        return response()->json(['info' => 'order changed succussfully', 'postID' => request('postID')]);
    }

    public function changeorder_vacancies()
    {
        $this->validate(request(),
            [
                'postID' => 'required',
                'orderID' => 'required'
            ]
        );
        $post_section = \App\Models\Vacancy::where('post_id', request('postID'))->update(['order_id' => request('orderID')]);
        return response()->json(['info' => 'order changed succussfully', 'postID' => request('postID')]);
    }

    public function changeorder_gallery()
    {
        $this->validate(request(),
            [
                'postID' => 'required',
                'orderID' => 'required'
            ]
        );
        $post_section = \App\Models\Gallery::where('id', request('postID'))->update(['order_id' => request('orderID')]);
        return response()->json(['info' => 'order changed succussfully', 'postID' => request('postID')]);
    }
    public function changeorder_pagegallery()
    {
        $this->validate(request(),
            [
                'postID' => 'required',
                'orderID' => 'required'
            ]
        );
        $post_section = \App\Models\PageGallery::where('id', request('postID'))->update(['order_id' => request('orderID')]);
        return response()->json(['info' => 'order changed succussfully', 'postID' => request('postID')]);
    }

    public function delete_post_from_section()
    {
        $this->validate(request(),
            [
                'postID' => 'required'
            ]
        );
        $post_section = \App\Models\News::where('post_id', request('postID'))->delete();
        return response()->json(['info' => 'post deleted succussfully']);
    }

    public function delete_note_from_section()
    {
        $this->validate(request(),
            [
                'postID' => 'required'
            ]
        );
        $post_section = \App\Models\Note::where('post_id', request('postID'))->delete();
        return response()->json(['info' => 'post deleted succussfully']);
    }

    public function delete_product_from_section()
    {
        $this->validate(request(),
            [
                'postID' => 'required'
            ]
        );
        $post_section = \App\Models\Product::where('post_id', request('postID'))->delete();
        return response()->json(['info' => 'post deleted succussfully']);
    }
    public function delete_vacancy_from_section()
    {
        $this->validate(request(),
            [
                'postID' => 'required'
            ]
        );
        $post_section = \App\Models\Vacancy::where('post_id', request('postID'))->delete();
        return response()->json(['info' => 'post deleted succussfully']);
    }

    public function delete_gallery_from_section()
    {
        $this->validate(request(),
            [
                'postID' => 'required'
            ]
        );
        $post_section = \App\Models\Gallery::where('id', request('postID'))->delete();
        return response()->json(['info' => 'post deleted succussfully']);
    }
    public function delete_page_gallery_from_section()
    {
        $this->validate(request(),
            [
                'postID' => 'required'
            ]
        );
        $post_section = \App\Models\PageGallery::where('id', request('postID'))->delete();
        return response()->json(['info' => 'post deleted succussfully']);
    }

    public function delete_service_from_section()
    {
        $this->validate(request(),
            [
                'postID' => 'required'
            ]
        );
        $post_section = \App\Models\Service::where('post_id', request('postID'))->delete();
        return response()->json(['info' => 'post deleted succussfully']);
    }


    public function drafts()
    {
        $posts = \App\Post::draftNotIn('postID', 'post_sections')->draftNotIn('articleID', 'post_articles')->get();
        $sections_posts = \App\Post::draftIn('postID', 'post_sections')->get();
        $sections = \App\Section::orderBy('order_by', 'asc')->get();
        return view('dashboard.draft', compact('posts', 'sections', 'sections_posts'))->with(['title' => 'A Panel']);
    }

    public function preview_draft($slug)
    {
        $post = \App\Post::where('slug', $slug)->where('langID', session('languageID'))->first();
        if (empty($post)) {
            $post = \App\Post::where('slug', $slug)->where('langID', 1)->firstOrFail();
        }

//For preview purpose
        $post_section = Post_section::where('section_id', 1)->where('postID', 1)->first();
        $post_section_prev = Post_section::where('section_id', $post_section->section_id)->where('order_id', $post_section->order_id - 1)->first();
        $post_section_next = Post_section::where('section_id', $post_section->section_id)->where('order_id', $post_section->order_id + 1)->first();
        $full_section = Post_section::where('section_id', 1)->get();
        $post_section_first = Post_section::where('section_id', 1)->where('order_id', $full_section->min('order_id'))->first();
        $post_section_last = Post_section::where('section_id', 1)->where('order_id', $full_section->max('order_id'))->first();

        return view('post.post', compact('post'))->with([
            "title" => $post->title,
            "description" => $post->metaDescription,
            "post_section" => $post_section,
            "post_section_prev" => $post_section_prev,
            "post_section_next" => $post_section_next,
            "full_section" => $full_section,
            "post_section_first" => $post_section_first,
            "post_section_last" => $post_section_last,

        ]);
    }

    public function add_news()
    {
        return view('dashboard.newpost33')->with(["title" => 'Insert New News', "description" => 'New Description']);
    }
    public function add_logo()
    {
        return view('dashboard.newpostlogo')->with(["title" => 'Insert Logo', "description" => 'New Logo']);
    }

    public function add_products()
    {
        return view('dashboard.newpost2')->with(["title" => 'Insert New Product', "description" => 'New Description']);
    }

    public function add_notes()
    {
        return view('dashboard.newpost3')->with(["title" => 'Insert New Product', "description" => 'New Description']);
    }
    public function add_services()
    {
        return view('dashboard.newpost5')->with(["title" => 'Insert New Product', "description" => 'New Description']);
    }

    public function add_vacancies()
    {
        return view('dashboard.newpost4')->with(["title" => 'Insert New Product', "description" => 'New Description']);
    }

    public function add_work()
    {
        $categories = \App\Category::where('type_id', 1)->orderBy('order_id', 'asc')->get();
        return view('dashboard.newpost', compact('categories'))->with(["title" => 'Insert New post', "description" => 'New Description']);
    }

    public function insert_post()
    {
        $validator = Validator::make(
            [
                'title' => request('title'),
                'subtitle' => request('subtitle'),
                'type_id' => request('type_id'),
                'order_id' => request('order_id'),
                'lang_id' => request('lang_id'),
            ],
            [
                'title' => ['required'],
                'subtitle' => ['nullable'],
                'type_id' => ['nullable', 'numeric'],
                'order_id' => ['nullable', 'numeric'],
                'lang_id' => ['nullable', 'numeric', 'exists:languages,lang_id'],
            ]
        );
        if ($validator->fails()) {
            return response(['result' => 'error', 'error' => $validator->messages()], 400);
        }
        $post = new \App\Models\News();
        $post->user_id = auth()->user()->id;
        $post->title = request('title');
        $post->subtitle = request('subtitle');
        $post->text = request('text');
        $post->description = request('description');
        $post->slug = request('slug');
        $post->type_id = request('type_id') ? request('type_id') : 1;
        $post->order_id = request('order_id') ? request('order_id') : 1;
        $post->lang_id = request('lang_id') ? request('lang_id') : 1;
        $post->save();
        //assign post_id
        $post = \App\Models\News::find($post->id);
        $post->post_id = $post->id;
        $post->save();
        if (request('default_lang') != 1) {
            $translatedPost = new \App\Models\News();
            $translatedPost->post_id = $post->post_id;
            $translatedPost->title = trim(request('title'));
            $translatedPost->subtitle = trim(request('subtitle'));
//        $translatedPost->metaDescription = request('metaDescription');
            $translatedPost->text = request('text');
            $translatedPost->description = request('description');
            $translatedPost->lang_id = request('default_lang');
            $translatedPost->slug = $post->slug;
            $translatedPost->order_id = $post->order_id;
            $translatedPost->type_id = $post->type_id;
            $translatedPost->save();
        }
        if (\request()->file()) {
            \request()->merge(['type_id' => 1]);
            $uploadFile = new UploadController();
            $uploadedFile = $uploadFile->uploadFile(request(), $post->post_id);
            return response()->json(['status' => $uploadedFile]);
        }


        return response()->json(['status' => 'success', 'result' => [
            'data' => $post]]);
    }
    public function insert_logo(Request $request)
    {
        $static_content = Static_content::where('page', 'all')->where('key', 'header-logo')->firstOrFail();
        if($request->hasFile('header')) {
            $file = $request->file('header');
            $file->move('uploads/', $file->getClientOriginalName());
            $static_content->content = \URL::to('uploads/' . $file->getClientOriginalName());
            $static_content->type_id = 2;
            $static_content->save();
        }
        $static_content = Static_content::where('page', 'all')->where('key', 'footer-logo')->firstOrFail();
        if($request->hasFile('footer')) {
            $file = $request->file('footer');
            $file->move('uploads/', $file->getClientOriginalName());
            $static_content->content = \URL::to('uploads/' . $file->getClientOriginalName());
            $static_content->type_id = 2;
            $static_content->save();
        }
        return response()->json(['status' => 'success', 'result' => [
            'data' => $static_content]]);
    }
    public function insert_product()
    {
        $validator = Validator::make(
            [
                'title' => request('title'),
                'subtitle' => request('subtitle'),
                'type_id' => request('type_id'),
                'order_id' => request('order_id'),
                'lang_id' => request('lang_id'),
            ],
            [
                'title' => ['required'],
                'subtitle' => ['nullable'],
                'type_id' => ['nullable', 'numeric'],
                'order_id' => ['nullable', 'numeric'],
                'lang_id' => ['nullable', 'numeric', 'exists:languages,lang_id'],
            ]
        );
        if ($validator->fails()) {
            return response(['result' => 'error', 'error' => $validator->messages()], 400);
        }
        $post = new \App\Models\Product();
        $post->user_id = auth()->user()->id;
        $post->title = request('title');
        $post->price = request('price');
        $post->subtitle = request('subtitle');
        $post->text = request('text');
        $post->description = request('description');
        $post->slug = request('slug');
        $post->type_id = request('type_id') ? request('type_id') : 1;
        $post->order_id = request('order_id') ? request('order_id') : 1;
        $post->lang_id = request('lang_id') ? request('lang_id') : 1;
        $post->save();
        //assign post_id
        $post = \App\Models\Product::find($post->id);
        $post->post_id = $post->id;
        $post->save();
        if (request('default_lang') != 1) {
            $translatedPost = new \App\Models\Product();
            $translatedPost->post_id = $post->post_id;
            $translatedPost->title = trim(request('title'));
            $translatedPost->price = trim(request('price'));
            $translatedPost->subtitle = trim(request('subtitle'));
//        $translatedPost->metaDescription = request('metaDescription');
            $translatedPost->text = request('text');
            $translatedPost->description = request('description');
            $translatedPost->lang_id = request('default_lang');
            $translatedPost->slug = $post->slug;
            $translatedPost->order_id = $post->order_id;
            $translatedPost->type_id = $post->type_id;
            $translatedPost->save();
        }
        if (\request()->file()) {
            \request()->merge(['type_id' => 1]);
            $uploadFile = new UploadController();
            $uploadedFile = $uploadFile->uploadFile(request(), $post->post_id);
            return response()->json(['status' => $uploadedFile]);
        }
        return response()->json(['status' => 'success', 'result' => [
            'data' => $post]]);
    }

    public function insert_note()
    {
        $validator = Validator::make(
            [
                'title' => request('title'),
                'subtitle' => request('subtitle'),
                'order_id' => request('order_id'),
                'lang_id' => request('lang_id'),
            ],
            [
                'title' => ['required'],
                'subtitle' => ['nullable'],
                'order_id' => ['nullable', 'numeric'],
                'lang_id' => ['nullable', 'numeric', 'exists:languages,lang_id'],
            ]
        );
        if ($validator->fails()) {
            return response(['result' => 'error', 'error' => $validator->messages()], 400);
        }
        $post = new \App\Models\Note();
        $post->user_id = auth()->user()->id;
        $post->title = request('title');
        $post->subtitle = request('subtitle');
        $post->description = request('description');
        $post->order_id = request('order_id') ? request('order_id') : 1;
        $post->lang_id = request('lang_id') ? request('lang_id') : 1;
        $post->save();
        //assign post_id
        $post = \App\Models\Note::find($post->id);
        $post->post_id = $post->id;
        $post->save();
        if (request('default_lang') != 1) {
            $translatedPost = new \App\Models\Note();
            $translatedPost->post_id = $post->post_id;
            $translatedPost->title = trim(request('title'));
            $translatedPost->subtitle = trim(request('subtitle'));
//        $translatedPost->metaDescription = request('metaDescription');
            $translatedPost->description = request('description');
            $translatedPost->lang_id = request('default_lang');
            $translatedPost->order_id = $post->order_id;
            $translatedPost->save();
        }
        if (\request()->file()) {
            \request()->merge(['type_id' => 1]);
            $uploadFile = new UploadController();
            $uploadedFile = $uploadFile->uploadFile(request(), $post->post_id);
            return response()->json(['status' => $uploadedFile]);
        }
        return response()->json(['status' => 'success', 'result' => [
            'data' => $post]]);
    }
    public function insert_services()
    {
        $validator = Validator::make(
            [
                'title' => request('title'),
                'subtitle' => request('subtitle'),
                'order_id' => request('order_id'),
                'lang_id' => request('lang_id'),
            ],
            [
                'title' => ['required'],
                'subtitle' => ['nullable'],
                'order_id' => ['nullable', 'numeric'],
                'lang_id' => ['nullable', 'numeric', 'exists:languages,lang_id'],
            ]
        );
        if ($validator->fails()) {
            return response(['result' => 'error', 'error' => $validator->messages()], 400);
        }
        $post = new \App\Models\Service();
        $post->user_id = auth()->user()->id;
        $post->title = request('title');
        $post->text = request('text');
        $post->description = request('description');
        $post->order_id = request('order_id') ? request('order_id') : 1;
        $post->lang_id = request('lang_id') ? request('lang_id') : 1;
        $post->save();
        //assign post_id
        $post = \App\Models\Service::find($post->id);
        $post->post_id = $post->id;
        $post->save();
        if (request('default_lang') != 1) {
            $translatedPost = new \App\Models\Note();
            $translatedPost->post_id = $post->post_id;
            $translatedPost->title = trim(request('title'));
//        $translatedPost->metaDescription = request('metaDescription');
            $translatedPost->text = request('text');
            $translatedPost->description = request('description');
            $translatedPost->lang_id = request('default_lang');
            $translatedPost->order_id = $post->order_id;
            $translatedPost->save();
        }
        if (\request()->file()) {
            \request()->merge(['type_id' => 1]);
            $uploadFile = new UploadController();
            $uploadedFile = $uploadFile->uploadFile(request(), $post->post_id);
            return response()->json(['status' => $uploadedFile]);
        }
        return response()->json(['status' => 'success', 'result' => [
            'data' => $post]]);
    }

    public function insert_vacancy()
    {
        $validator = Validator::make(
            [
                'title' => request('title'),
                'order_id' => request('order_id'),
                'lang_id' => request('lang_id'),
            ],
            [
                'title' => ['required'],
                'order_id' => ['nullable', 'numeric'],
                'lang_id' => ['nullable', 'numeric', 'exists:languages,lang_id'],
            ]
        );
        if ($validator->fails()) {
            return response(['result' => 'error', 'error' => $validator->messages()], 400);
        }
        $post = new \App\Models\Vacancy();
        $post->user_id = auth()->user()->id;
        $post->title = request('title');
        $post->description = request('description');
        $post->order_id = request('order_id') ? request('order_id') : 1;
        $post->expiration_date = request('exp_date');
        $post->lang_id = request('lang_id') ? request('lang_id') : 1;
        $post->save();
        //assign post_id
        $post = \App\Models\Vacancy::find($post->id);
        $post->post_id = $post->id;
        $post->save();
        if (request('default_lang') != 1) {
            $translatedPost = new \App\Models\Vacancy();
            $translatedPost->post_id = $post->post_id;
            $translatedPost->title = trim(request('title'));
//        $translatedPost->metaDescription = request('metaDescription');
            $translatedPost->description = request('description');
            $translatedPost->lang_id = request('default_lang');
            $translatedPost->expiration_date = request('exp_date');
            $translatedPost->order_id = $post->order_id;
            $translatedPost->save();
        }
        return response()->json(['status' => 'success', 'result' => [
            'data' => $post]]);
    }

    public function insert_post_text()
    {
        $this->validate(request(), [
            'post_id' => 'required|exists:posts,id',
            'upl.*' => 'image|mimes:jpeg,jpg,png,gif,bmp|max:5000',
            'title' => '',
            'postText' => 'required',
            'leftOrRight' => 'required|in:left,right'
        ]);

        $post_text = new Post_text();
        $post_text->title = request('title');
        $post_text->text = request('postText');
        $post_text->leftOrRight = request('leftOrRight');
        $post_text->save();

        $published_text = new Published_text();
        $published_text->post_textID = $post_text->id;
        $published_text->contentID = request('post_id');
        $published_text->save();

        if (request()->hasFile('upl')) {
            $images = request()->file('upl');
            foreach ($images as $image) {
                $name = 'assets/images/upl/' . time() . '-' . $image->getClientOriginalName();
                $image->move('assets/images/upl/', $name);
                $photo = new \App\Image();
                $photo->image = $name;
                $photo->image_thumbnail = $name;
                $photo->save();
                $post_texts_image = new \App\Post_texts_image();
                $post_texts_image->imageID = $photo->id;
                $post_texts_image->contentID = $post_text->id;
                $post_texts_image->image_genderID = request('image_genderID');
                $post_texts_image->save();
            }
            return response()->json(['status' => 'success']);
        }


        //add posts texts id
        $post_texts_update_id = \App\Post_text::find($post_text->id);
        $post_texts_update_id->post_texts_id = $post_text->id;
        $post_texts_update_id->save();

        return response()->json(['info' => 'Post text succussfully inserted ']);
    }

    public function delete_post_text()
    {
        $this->validate(request(), [
            'post_text_id' => 'required|exists:published_texts,post_textID',
        ]);

        $published_text = Published_text::where('post_textID', request('post_text_id'))->firstOrFail();
        $published_text->delete();

        return response()->json(['info' => 'Post text succussfully Removed ']);
    }

    public function delete_post_text_image()
    {
        $this->validate(request(), [
            'imageID' => 'required|exists:post_texts_images,id',
        ]);

        $published_text_image = Post_texts_image::findOrFail(request('imageID'));
        $published_text_image->delete();

        return response()->json(['info' => 'Post text image succussfully Removed ']);
    }

    public function update_post_text()
    {
        $this->validate(request(), [
            'post_text_id' => 'required|exists:published_texts,post_textID',
            'upl.*' => 'image|mimes:jpeg,jpg,png,gif,bmp|max:5000',
            'title' => 'required',
            'postText' => 'required',
            'leftOrRight' => 'required|in:left,right'
        ]);

        $post_text = Post_text::find(request('post_text_id'));
        $post_text->title = request('title');
        $post_text->text = request('postText');
        $post_text->leftOrRight = request('leftOrRight');
        $post_text->breadcrumbs = request('breadcrumbs') !== null ? request('breadcrumbs') : 1;
        $post_text->save();

        $published_text = Published_text::where('post_textID', request('post_text_id'))->first();
        $published_text->order_id = request('order_id');
        $published_text->save();

        if (request()->hasFile('upl')) {
            $images = request()->file('upl');
            foreach ($images as $image) {
                $name = 'assets/images/upl/' . time() . '-' . $image->getClientOriginalName();
                $image->move('assets/images/upl/', $name);
                $photo = new \App\Image();
                $photo->image = $name;
                $photo->image_thumbnail = $name;
                $photo->save();
                $post_texts_image = new \App\Post_texts_image();
                $post_texts_image->imageID = $photo->id;
                $post_texts_image->contentID = $post_text->id;
                $post_texts_image->image_genderID = request('image_genderID');
                $post_texts_image->save();
            }
            return response()->json(['status' => 'success']);
        }

        return response()->json(['info' => 'Post text succussfully inserted ']);
    }


    public function publishpost()
    {
        $this->validate(request(),
            [
                'postID' => 'required',
                'sectionID' => 'required',
                'orderID' => 'required'
            ]
        );
        $post_section = new \App\Post_section;
        $post_section->section_id = request('sectionID');
        $post_section->postID = request('postID');
        $post_section->order_id = request('orderID');
        $post_section->save();
        return response()->json(['info' => 'draft published succussfully']);
    }

    public function publish_article()
    {
        $this->validate(request(),
            [
                'postID' => 'required',
                'articleID' => 'required',
                'orderID' => 'required'
            ]
        );
        $post_article = new \App\Post_article;
        $post_article->articleID = request('articleID');
        $post_article->postID = request('postID');
        $post_article->order_id = request('orderID');
        $post_article->save();
        return response()->json(['info' => 'draft published succussfully']);
    }

    public function deletepostfromdrafts()
    {
        $this->validate(request(),
            [
                'postID' => 'required'
            ]
        );

        $draft = \App\Post::where('id', request('postID'))->firstOrFail();
        $draft->delete();
        return response()->json(['info' => 'draft deleted succussfully']);
    }

    public function post($slug)
    {
        if (empty($post)) {
            $post = \App\Models\News::where('slug', $slug)->where('lang_id', 1)->firstOrFail();
        }
//        $categories = \App\Models\Category::where('type_id',$post->type_id)->orderBy('order_id','asc')->get();
        return view('dashboard.post', compact('post'))->with(["title" => $post->title, "description" => $post->metaDescription]);
    }

    public function service($id)
    {
        if (empty($post)) {
            $post = \App\Models\Service::where('id', $id)->where('lang_id', 1)->firstOrFail();
        }
//        $categories = \App\Models\Category::where('type_id',$post->type_id)->orderBy('order_id','asc')->get();
        return view('dashboard.service', compact('post'))->with(["title" => $post->title, "description" => $post->metaDescription]);
    }

    public function products($slug)
    {
        if (empty($post)) {
            $post = \App\Models\Product::where('slug', $slug)->where('lang_id', 1)->firstOrFail();
        }
//        $categories = \App\Models\Category::where('type_id',$post->type_id)->orderBy('order_id','asc')->get();
        return view('dashboard.product', compact('post'))->with(["title" => $post->title, "description" => $post->metaDescription]);
    }

    public function notes($id)
    {
        if (empty($post)) {
            $post = \App\Models\Note::where('id', $id)->where('lang_id', 1)->firstOrFail();
        }
//        $categories = \App\Models\Category::where('type_id',$post->type_id)->orderBy('order_id','asc')->get();
        return view('dashboard.note', compact('post'))->with(["title" => $post->title, "description" => $post->metaDescription]);
    }

    public function vacancies($id)
    {
        if (empty($post)) {
            $post = \App\Models\Vacancy::where('id', $id)->where('lang_id', 1)->firstOrFail();
        }
//        $categories = \App\Models\Category::where('type_id',$post->type_id)->orderBy('order_id','asc')->get();
        return view('dashboard.vacancy', compact('post'))->with(["title" => $post->title, "description" => $post->metaDescription]);
    }

    public function translates($post_id, $langID)
    {
        $post = \App\Post::where('post_id', $post_id)->where('lang_id', $langID)->first();
        $post_original = \App\Post::where('post_id', $post_id)->where('lang_id', 1)->first();
        return view('dashboard.translates', compact('post', 'post_original'))->with(["title" => 'translate', "description" => 'translate']);
    }

    public function text_translates($text_id, $langID)
    {
        $post = \App\Post_text::where('post_texts_id', $text_id)->where('langID', $langID)->first();
        $post_original = \App\Post_text::where('post_texts_id', $text_id)->where('langID', 1)->first();
        return view('dashboard.text_translates', compact('post', 'post_original'))->with(["title" => 'translate', "description" => 'translate']);
    }

    public function update_translates($post_id, $langID)
    {

        $this->validate(request(),
            [
                'title' => 'required',
                'subtitle' => 'required',
                'metaDescription' => 'required',
            ]
        );

        $post_original = \App\Post::where('post_id', $post_id)->where('lang_id', 1)->first();

        $post = \App\Post::where('post_id', $post_id)->where('lang_id', $langID)->first();
        if (empty($post)) {
            $post = new \App\Post;
        }
        $post->post_id = $post_id;
        $post->title = trim(request('title'));
        $post->subtitle = trim(request('subtitle'));
//        $post->metaDescription = request('metaDescription');
        $post->text = request('text');
        $post->lang_id = $langID;
        $post->slug = $post_original->slug;
        $post->order_id = $post_original->order_id;
        $post->is_pinned = $post_original->is_pinned;
        $post->type_id = $post_original->type_id;
        $post->date = $post_original->date;
        $post->save();

        return response()->json(['info' => 'translated']);
    }

    public function update_text_translates($text_id, $langID)
    {

        $this->validate(request(), [
            'post_texts_id' => 'required|exists:post_texts,post_texts_id',
            'title' => 'required',
            'postText' => 'required',
        ]);

        $post_original = \App\Post_text::where('post_texts_id', $text_id)->where('langID', 1)->first();
        $post_text = \App\Post_text::where('post_texts_id', $text_id)->where('langID', $langID)->first();
        if (empty($post)) {
            $post_text = new \App\Post_text;
        }
        $post_text->post_texts_id = $text_id;
        $post_text->title = request('title');
        $post_text->text = request('postText');
        $post_text->leftOrRight = $post_original->leftOrRight;
        $post_text->langID = $langID;
        $post_text->save();

        return response()->json(['info' => 'translated']);
    }


    /*
     * Delete Post
     */
    public function delete_post()
    {
        $this->validate(request(),
            [
                'post_id' => 'required',
            ]
        );
        $post_sections = \App\Post_section::where('postID', request('post_id'))->get();

        foreach ($post_sections as $post_section) {
            $post_section->delete();
        }

        $images = \App\Image::where('contentID', request('contentID'))->get();

        foreach ($images as $image) {
            $image->delete();
        }

        $post = \App\Post::where('id', request('post_id'))->where('langID', 1)->firstOrFail();
        $post->delete();
        return response()->json(['info' => 'Post Deleted']);
    }

    /*
     * Update Post
     */
    public function update_post()
    {
        $this->validate(request(),
            [
                'post_id' => 'required',
                'title' => 'required|max:191',
                'subtitle' => 'required|max:191',
                'text' => 'required',
                'description' => 'required',
                'slug' => 'required|max:191',
            ]
        );

//        SECTION CONTROL
//        END SECTION CONTROL
        $post = \App\Models\News::where('id', request('post_id'))->where('lang_id', 1)->firstOrFail();
        $post->title = request('title');
        $post->user_id = auth()->user()->id;
        $post->subtitle = request('subtitle');
        $post->text = request('text');
        $post->description = request('description');
        $post->slug = request('slug');
        $post->save();
        if (request('default_lang') != 1) {
            $translatedPost = new \App\Models\News();
            if (\App\Models\News::where('post_id', request('post_id'))->where('lang_id', request('default_lang'))->exists()) {
                $translatedPost = \App\Models\News::where('post_id', request('post_id'))->where('lang_id', request('default_lang'))->first();
            }
            $translatedPost->post_id = $post->post_id;
            $translatedPost->title = trim(request('title'));
            $translatedPost->user_id = auth()->user()->id;
            $translatedPost->subtitle = trim(request('subtitle'));
//        $translatedPost->metaDescription = request('metaDescription');
            $translatedPost->text = request('text');
            $translatedPost->description = request('description');
            $translatedPost->lang_id = request('default_lang');
            $translatedPost->slug = $post->slug;
            $translatedPost->order_id = $post->order_id;
            $translatedPost->type_id = $post->type_id;
            $translatedPost->save();
        }

        return response()->json(['info' => 'Post Updated']);
    }

    public function update_service()
    {
        $this->validate(request(),
            [
                'post_id' => 'required',
                'title' => 'required|max:191',
                'postText' => 'required',
            ]
        );

//        SECTION CONTROL
//        END SECTION CONTROL
        $post = \App\Models\Service::where('id', request('post_id'))->where('lang_id', 1)->firstOrFail();
        $post->title = request('title');
        $post->user_id = auth()->user()->id;
        $post->text = request('postText');
        $post->description = request('description');
        $post->save();
        if (request('default_lang') != 1) {
            $translatedPost = new \App\Models\Service();
            if (\App\Models\Service::where('post_id', request('post_id'))->where('lang_id', request('default_lang'))->exists()) {
                $translatedPost = \App\Models\Service::where('post_id', request('post_id'))->where('lang_id', request('default_lang'))->first();
            }
            $translatedPost->post_id = $post->post_id;
            $translatedPost->title = trim(request('title'));
            $translatedPost->user_id = auth()->user()->id;
//        $translatedPost->metaDescription = request('metaDescription');
            $translatedPost->text = request('text');
            $translatedPost->description = request('description');
            $translatedPost->lang_id = request('default_lang');
            $translatedPost->order_id = $post->order_id;
            $translatedPost->save();
        }

        return response()->json(['info' => 'Post Updated']);
    }

    public function update_product()
    {
        $this->validate(request(),
            [
                'post_id' => 'required',
                'title' => 'required|max:191',
                'subtitle' => 'required|max:191',
                'postText' => 'required',
                'slug' => 'required|max:191',
            ]
        );

//        SECTION CONTROL
//        END SECTION CONTROL
        $post = \App\Models\Product::where('id', request('post_id'))->where('lang_id', 1)->firstOrFail();
        $post->title = request('title');
        $post->price = request('price');
        $post->user_id = auth()->user()->id;
        $post->subtitle = request('subtitle');
        $post->text = request('postText');
        $post->slug = request('slug');
        $post->type_id = request('type_id');
        $post->save();
        if (request('default_lang') != 1) {
            $translatedPost = new \App\Models\Product();
            if (\App\Models\Product::where('post_id', request('post_id'))->where('lang_id', request('default_lang'))->exists()) {
                $translatedPost = \App\Models\Product::where('post_id', request('post_id'))->where('lang_id', request('default_lang'))->first();
            }
            $translatedPost->post_id = $post->post_id;
            $translatedPost->title = trim(request('title'));
            $translatedPost->user_id = auth()->user()->id;
            $translatedPost->subtitle = trim(request('subtitle'));
//        $translatedPost->metaDescription = request('metaDescription');
            $translatedPost->text = request('text');
            $translatedPost->lang_id = request('default_lang');
            $translatedPost->slug = $post->slug;
            $translatedPost->order_id = $post->order_id;
            $translatedPost->type_id = $post->type_id;
            $translatedPost->save();
        }

        return response()->json(['info' => 'Post Updated']);
    }

    public function update_note()
    {
        $this->validate(request(),
            [
                'title' => 'required|max:191',
                'subtitle' => 'required|max:191',
            ]
        );

//        SECTION CONTROL
//        END SECTION CONTROL
        $post = \App\Models\Note::where('id', request('post_id'))->where('lang_id', 1)->firstOrFail();
        $post->title = request('title');
        $post->user_id = auth()->user()->id;
        $post->subtitle = request('subtitle');
        $post->description = request('postText');
        $post->save();
        if (request('default_lang') != 1) {
            $translatedPost = new \App\Models\Note();
            if (\App\Models\Note::where('post_id', request('post_id'))->where('lang_id', request('default_lang'))->exists()) {
                $translatedPost = \App\Models\Note::where('post_id', request('post_id'))->where('lang_id', request('default_lang'))->first();
            }
            $translatedPost->post_id = $post->post_id;
            $translatedPost->title = trim(request('title'));
            $translatedPost->user_id = auth()->user()->id;
            $translatedPost->subtitle = trim(request('subtitle'));
            $translatedPost->description = request('postText');
//        $translatedPost->metaDescription = request('metaDescription');
            $translatedPost->lang_id = request('default_lang');
            $translatedPost->order_id = $post->order_id;
            $translatedPost->save();
        }

        return response()->json(['info' => 'Post Updated']);
    }

    public function update_vacancy()
    {
        $this->validate(request(),
            [
                'title' => 'required|max:191',
            ]
        );

//        SECTION CONTROL
//        END SECTION CONTROL
        $post = \App\Models\Vacancy::where('id', request('post_id'))->where('lang_id', 1)->firstOrFail();
        $post->title = request('title');
        $post->user_id = auth()->user()->id;
        $post->description = request('description');
        $post->expiration_date = request('exp_date');
        $post->save();
        if (request('default_lang') != 1) {
            $translatedPost = new \App\Models\Vacancy();
            if (\App\Models\Vacancy::where('post_id', request('post_id'))->where('lang_id', request('default_lang'))->exists()) {
                $translatedPost = \App\Models\Vacancy::where('post_id', request('post_id'))->where('lang_id', request('default_lang'))->first();
            }
            $translatedPost->post_id = $post->post_id;
            $translatedPost->title = trim(request('title'));
            $translatedPost->user_id = auth()->user()->id;
            $translatedPost->description = request('description');
//        $translatedPost->metaDescription = request('metaDescription');
            $translatedPost->lang_id = request('default_lang');
            $translatedPost->expiration_date = request('exp_date');
            $translatedPost->order_id = $post->order_id;
            $translatedPost->save();
        }

        return response()->json(['info' => 'Post Updated']);
    }

    /*
     * Post's IMAGE manipulation
     */
    public function upload_image()
    {
        // A list of permitted file extensions
        $this->validate(request(), [
            'post_id' => 'required',
            'upl.*' => 'image|mimes:jpeg,jpg,png,gif,bmp|max:5000'
        ]);

        if (\request()->file()) {
            $uploadFile = new UploadController();
            $uploadedFile = $uploadFile->uploadFile(request(), request('post_id'));
            return response()->json(['status' => $uploadedFile]);
        }
        return response()->json(['status' => 'error']);
    }

    /*
     * Post's IMAGE manipulation
     */
    public function upload_gallery(Request $request)
    {
        // A list of permitted file extensions

        if ($request->hasFile('upl')) {

            $request->validate([
                'upl' => 'required|image|mimes:jpeg,jpg,png,gif,bmp|max:5000'
            ]);
            $time = date('Y-m-d_H-i-s');
            $file = $request->file('upl');
            $file->move('galleries', $time . '-' . $file->getClientOriginalName());

            // Store the record, using the new file hashname which will be it's new filename identity.
            $gallery = new \App\Models\Gallery([
                "image" => \URL::to('galleries/' . $time . '-' . $file->getClientOriginalName()),
                "alt" => $file->getClientOriginalName(),
            ]);
            $gallery->save(); // Finally, save the record.
            return response()->json(['status' => $gallery]);
        }
        return response()->json(['status' => 'error']);
    }
    public function upload_page_gallery(Request $request)
    {
        // A list of permitted file extensions

        if ($request->hasFile('upl')) {

            $request->validate([
                'upl' => 'required'
            ]);
            $time = date('Y-m-d_H-i-s');
            $file = $request->file('upl');
            $file->move('galleries', $time . '-' . $file->getClientOriginalName());

            // Store the record, using the new file hashname which will be it's new filename identity.
            $gallery = new \App\Models\PageGallery([
                "page"=>request('page'),
                "content" => \URL::to('galleries/' . $time . '-' . $file->getClientOriginalName()),
                "alt" => $file->getClientOriginalName(),
            ]);
            $gallery->save(); // Finally, save the record.
            return response()->json(['status' => $gallery]);
        }
        return response()->json(['status' => 'error']);
    }

    public function tiny_photo_upload()
    {
        $image = request()->file('file');
        $name = \URL::to('assets/images/upl/' . time() . '-' . $image->getClientOriginalName());
        $image->move('assets/images/upl/', $name);

        return json_encode(array('location' => $name));
    }

    public function change_image_order()
    {
        $this->validate(request(),
            [
                'imageID' => 'required',
                'orderID' => 'required'
            ]
        );
        $image = \App\Published_image::where('imageID', request('imageID'))->firstOrFail();
        $image->order_id = request('orderID');
        $image->save();
        return response()->json(['info' => 'Order Changed']);
    }

    public function make_default_image()
    {
        $this->validate(request(),
            [
                'imageID' => 'required',
                'contentID' => 'required'
            ]
        );
        $images = \App\Published_image::where('contentID', request('contentID'))->get();
        foreach ($images as $image) {
            if ($image->defaultOrNot == 2) {
                $image->defaultOrNot = 1;
                $image->save();
            }
        }
        $image = \App\Published_image::where('imageID', request('imageID'))->where('contentID', request('contentID'))->firstOrFail();
        $image->defaultOrNot = 2;
        $image->save();
        return response()->json(['info' => 'Image set to default']);
    }

    public function delete_image()
    {
        $this->validate(request(),
            [
                'imageID' => 'required',
                'contentID' => 'required'
            ]
        );
        $image = \App\Models\Upload_node::where('relation_table', request('relation_table'))->where('file_id', request('imageID'))->where('data_id', request('contentID'))->delete();
        return response()->json(['info' => 'Image Deleted Succussfully']);
    }

    /*
    *  Cover Image
    */
    public function cover_images()
    {
        $cover_images = \App\Cover_image::get();
        return view('dashboard.cover_image', compact('cover_images'))->with(["title" => "Cover Images", "description" => ""]);
    }


    public function delete_cover_images()
    {
        $this->validate(request(),
            [
                'image_id' => 'required',
            ]
        );
        $image = \App\Cover_image::where('image_id', request('image_id'))->firstOrFail();
        $image->delete();
        return response()->json(['info' => 'Cover Image Deleted Succussfully']);
    }

    public function upload_cover_images()
    {
        // A list of permitted file extensions
        $this->validate(request(), [
            'upl.*' => 'required|image|mimes:jpeg,jpg,png,gif,bmp|max:55000'
        ]);

        if (request()->hasFile('upl')) {
            $images = request()->file('upl');
            foreach ($images as $image) {
                $name = 'assets/images/cover_images/' . time() . '-' . $image->getClientOriginalName();
                $image->move('assets/images/cover_images/', $name);
                $photo = new \App\Cover_image;
                $photo->image = $name;
                $photo->langID = 1;
                $photo->save();
                $update_id = \App\Cover_image::find($photo->id);
                $update_id->image_id = $photo->id;
                $update_id->save();
            }
            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'error']);
    }

    /*
     *  AUTHENTIFICATION
     */
    public function signin()
    {
        $credentialsWithEmail = array('email' => request('email'), 'password' => request('password'));

        $rememberMe = false;
//        if(request()->get('rememberme') != null){
//            $rememberMe = true;
//        }
        $user = User::where('email', request('email'))->firstOrFail();
        if (!auth()->attempt($credentialsWithEmail, $rememberMe)) {
            return 'error';
        }

        return redirect('apanel');
    }

    public function logout()
    {

        auth()->logout();

        return redirect('login');
    }
}
