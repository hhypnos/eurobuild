<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\Language;
use App\Models\News;
use App\Models\Note;
use App\Models\Order;
use App\Models\PageGallery;
use App\Models\Product;
use App\Models\Service;
use App\Models\Vacancy;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function __construct()
    {
        if (session('languageID') === null) {
            session(['languageID' => 1]);
        }
    }

    public function index()
    {
        $sessionLanguage = session('languageID');
        $news = News::with(['images.full_files', 'covers.full_files'])->orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(10)->get();
        $notes = Note::with(['images.full_files'])->orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(10)->get();
        $vacancies = Vacancy::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(5)->get();
        $services = Service::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(5)->get();
        $page_galleries = PageGallery::orderBy('order_id', 'asc')->where('page', 'home')->get();
        return view('welcome.index', compact('news', 'notes', 'vacancies', 'services','page_galleries'))->with(['title' => translate("EURO BUILD", session('languageID'))]);
    }

    public function products()
    {
        $sessionLanguage = session('languageID');
        $products = Product::with(['images.full_files', 'covers.full_files'])->orderBy('order_id', 'asc')->where('lang_id', $sessionLanguage)->paginate(25);
        if (request('cat')) {
            $products = Product::with(['images.full_files', 'covers.full_files'])->orderBy('order_id', 'asc')->where('lang_id', $sessionLanguage)->where('type_id', request('cat'))->paginate(25);
        }
        $services = Service::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(5)->get();
        return view('welcome.products', compact('products', 'services'))->with(['title' => translate("EURO BUILD", session('languageID'))]);
    }

    public function products_inner($slug)
    {
        $sessionLanguage = session('languageID');
        $product = Product::with(['images.full_files', 'covers.full_files'])->orderBy('order_id', 'asc')->where('slug', $slug)->where('lang_id', $sessionLanguage)->firstOrFail();
        $products = Product::with(['images.full_files', 'covers.full_files'])->orderBy('created_at', 'desc')->where('post_id', '!=', $product->post_id)->where('lang_id', $sessionLanguage)->where('type_id', $product->type_id)->limit(10)->get();
        $services = Service::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(5)->get();
        return view('welcome.products_inner', compact('product', 'products', 'services'))->with(['title' => translate("EURO BUILD", session('languageID'))]);
    }

    public function news()
    {
        $sessionLanguage = session('languageID');
        $news = News::with(['images.full_files', 'covers.full_files'])->orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->paginate(25);
        $services = Service::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(5)->get();
        return view('welcome.news', compact('news', 'services'))->with(['title' => translate("EURO BUILD", session('languageID'))]);
    }

    public function news_inner($slug)
    {
        $sessionLanguage = session('languageID');
        $news = News::with(['images.full_files', 'covers.full_files'])->orderBy('created_at', 'desc')->where('slug', $slug)->where('lang_id', $sessionLanguage)->first();
        $newses = News::with(['images.full_files', 'covers.full_files'])->orderBy('created_at', 'desc')->where('post_id', '!=', $news->post_id)->where('lang_id', $sessionLanguage)->limit(3)->get();
        $services = Service::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(5)->get();
        return view('welcome.news_inner', compact('news', 'newses', 'services'))->with(['title' => translate("EURO BUILD", session('languageID'))]);
    }

    public function about()
    {
        $sessionLanguage = session('languageID');
        $galleries = Gallery::orderBy('order_id', 'asc')->get();
        $services = Service::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(5)->get();
        return view('welcome.about', compact('galleries', 'services'))->with(['title' => translate("EURO BUILD", session('languageID'))]);
    }

    public function vacancy()
    {
        $sessionLanguage = session('languageID');
        $vacancies = Vacancy::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->get();
        $services = Service::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(5)->get();
        return view('welcome.vacancy', compact('vacancies', 'services'))->with(['title' => translate("EURO BUILD", session('languageID'))]);
    }

    public function gallery()
    {
        $sessionLanguage = session('languageID');
        $galleries = Gallery::orderBy('order_id', 'asc')->get();
        $services = Service::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(5)->get();
        return view('welcome.gallery', compact('galleries', 'services'))->with(['title' => translate("EURO BUILD", session('languageID'))]);
    }


    public function contact()
    {
        $sessionLanguage = session('languageID');
        $services = Service::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(5)->get();
        return view('welcome.contact', compact('services'))->with(['title' => translate("Contact With Us", session('languageID'))]);
    }

    public function send_email()
    {
        \Mail::send(array(), array(), function ($message) {
            $message->to(env('MAIL_USERNAME'))
                ->subject(env('MAIL_SUBJECT'))
                ->from(request('email'))
                ->setBody("full_name:" . request('full_name') . "<br> phone:" . request('phone') . "<br> Message:" . request('message'), 'text/html');
        });
        return redirect()->back()->withSuccess(['open' => 'open', 'msg' => translate("იმეილი გაიგზავნა", session('languageID'))]);

    }

    public function send_order()
    {
        $order = new Order();
        $order->phone = request('phone');
        $order->address = request('address');
        $order->is_delivery = request('is_delivery') == "on" ? 1 : 0;
        $order->is_download = request('is_download') == "on" ? 1 : 0;
        $order->message = request('message');
        $order->save();
        return redirect()->back()->withSuccess(['open' => 'open', 'msg' => translate("შეკვეთა წარმატებით განხორციელდა", session('languageID'))]);

    }

    /**
     * @param $lang
     * @return translated text
     **/
    public function language($lang)
    {
        session(['languageID' => $lang]);
        return redirect(url()->to('/'));
    }
}
