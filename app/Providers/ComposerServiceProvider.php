<?php

namespace App\Providers;

use App\Models\Navigation;
use App\Models\Service;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $navigation = new \App\Models\Navigation();
        view()->composer(['layouts.*','welcome.*','errors.*'],  function(View $view) use ($navigation){
            $sessionLanguage = session('languageID');
            if(!Navigation::where('lang_id',$sessionLanguage)->exists()){
                $sessionLanguage = 1;
            }
            $view->with('languages', \App\Models\Language::get());
	    $view->with('navigations', $navigation->fetchRecursively(0,'ASC',$sessionLanguage ?? 1));
	    $page_cover = \App\Models\PageGallery::where('page',request()->segment(count(request()->segments())))->orderBy('id','desc')->first();
	    if(empty($page_cover)){
		
$page_cover = \App\Models\PageGallery::where('page','products_inner')->orderBy('id','desc')->first();
	    }
            $view->with('page_cover', $page_cover);
            $services = Service::orderBy('created_at', 'desc')->where('lang_id', $sessionLanguage)->limit(5)->get();
            $view->with('services', $services);
            $view->with('static_contents', function($page,$key){ return translate(!empty(\App\Models\Static_content::where('page',$page)->where('key',$key)->where('lang_id',session('languageID'))->first()) ? \App\Models\Static_content::where('page',$page)->where('key',$key)->where('lang_id',session('languageID'))->first() : \App\Models\Static_content::where('page',$page)->where('key',$key)->where('lang_id',1)->first(), session("languageID") ?? 1);});
        });

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
